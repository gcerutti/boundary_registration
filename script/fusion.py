#!/usr/bin/env python

import os
import argparse
import time
import logging
import pandas as pd
import matplotlib.pyplot as plt

from titk_tools.io import imread, imsave, tfread, tfsave, read_trsf, save_trsf
from timagetk.plugins.fusion import fusion


logging.getLogger().setLevel(logging.INFO)

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('data_fname', help='path to a textfile containing the list of image filenames')
parser.add_argument('-imgdir', '--img-dirname', help='path to the directory of images to register', type=str, default='.')
args = parser.parse_args()

start = time.time()

fname = args.data_fname
dirname = args.img_dirname


###################################################
# Input data:
###################################################

# filenames
fused_name = "fused_"

# images to register
df=pd.read_csv(fname, delimiter=',')

file_no = list(df['no'])
imgnames = list(df['imgname'])

#filenames = [fused_name + "_" + str(t).zfill(2) + '_' for t in file_no]

outputdir='.'

# -- "Specify paths to image files" --
# ------------------------------------------------
images = []
for i in range(len(imgnames)):
	filename=imgnames[i]
	image_filename = dirname + '/' + imgnames[i]
	images.append(imread(image_filename))

fused_img=fusion(images, iterations=2, mean_imgs_prefix=fused_name)

imsave(outputdir+'/'+fused_name+'last.inr.gz', fused_img)
