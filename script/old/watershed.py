#!/usr/bin/env python

# Usage :
# ./segmentation_originale_titk.py img_fuzed.inr.gz 
#####################################################

from sys import path, argv
import os

import numpy as np

from titk_tools.io import imread, imsave
from titk_tools.segmentation import filtering, segmentation_watershed

#############################
# filename definitions :
#############################
filename = argv[1]

imname = filename.split('/')[-1]
imnameroot = imname.split('.')[0]

outputdir = imnameroot + '_wat'
# create a directory and put all the result files in it
os.system("mkdir "+outputdir)
print("results are written in the directory ",outputdir)

#############################
# parameters :
#############################

filter_type_seeds = "asf"
filter_value_seeds = 3

filter_type_wat = "gaussian"
filter_value_wat = 3

voxel_volmin = 2000

#h_minima_values = [2, 3, 4]
h_minima_values = [2]

############ Main ###########

image=imread(filename)

# Filtering
# -----------------------------
img_seeds = filtering(image, filter_type_seeds, filter_value_seeds)
#img_wat = filtering(image, filter_type_wat, filter_value_wat)
img_wat = image

# Seed extraction and watershed
# -----------------------------
for h_minima in h_minima_values :
	print("hmin =", h_minima)
	param_string = '_seeds-'+filter_type_seeds+str(filter_value_seeds)+'_wat-'+filter_type_wat+str(filter_value_wat)+'_hmin'+str(h_minima)
	wat, seeds = segmentation_watershed(img_seeds, img_wat, h_minima, voxel_volmin)
	#saving the watershed
	wat_contour_name = outputdir + '/' + imnameroot+param_string + '_wat.inr.gz'
	imsave(wat_contour_name, wat)
	#saving the seeds
	seeds_name = outputdir + '/' + imnameroot+param_string + '_seeds.inr.gz'
	imsave(seeds_name, seeds)
	print("There are ", len(np.unique(wat)), " cells detected.")

