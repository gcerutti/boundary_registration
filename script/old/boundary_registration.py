#!/usr/bin/env python

import os
from sys import argv
import numpy as np
import time

#from bib_titk import *
from titk_tools.rw.registration import find_rigid_transfo2, find_affine_transfo, find_nl_transfo

'''
find_rigid_transfo2('2013-03-05_LTi6B_dis_A_T12h.tif', '2013-03-05_LTi6B_dis_A_T0.tif', 'res.inr.gz', 'tf.txt')
find_affine_transfo('2013-03-05_LTi6B_dis_A_T12h.tif', '2013-03-05_LTi6B_dis_A_T0.tif', 'res_affine.inr.gz', 'tf.txt', 'tf_affine.txt')
find_nl_transfo('2013-03-05_LTi6B_dis_A_T12h.tif', 'res_affine.inr.gz', 'res_nl.inr.gz',  'tf_nl.inr.gz')

if __name__ == "__main__":
    print("Registration")
'''


start = time.time()

es = int(argv[1])#5
fs = int(argv[2])#3

###################################################
# Input data:
###################################################

# times to register
tn = [0,1,2,3,4,5]
tlabels = ['0', '12h', '24h', '36h', '48h']
N_times = len(tn)

# filenames
root="2013-03-05_LTi6B_dis_A_T"

def fname(i):
	return root+tlabels[i]+'.tif'
	
# segmented image filenames
def wfname(i):
	return root+tlabels[i]+"_wat.inr.gz"


# if we want to register (i+1) on (i)	
#pair_list = [ [i,i+1] for i in range(0,N_times-2)]

# if we want to register (i) on (i+1)	
pair_list = [ [i+1,i] for i in range(0,N_times-2)]

for pair in pair_list :
	###################################################
	# Parameters:
	###################################################

	# blockmatching parameters for local registration
	# ----------------------------------------------
	ph = 6 #  pyramid-highest-level
	# default is 3: it corresponds to 32x32x32 for an original 256x256x256 image

	pl = 1 # pyramid-lowest-level
	# default is 0: original dimension

	#es = 5 # elastic-sigma
	# sigma for elastic regularization of the transformation

	#fs = 3 # fluid-sigma
	# sigma for fluid regularization (field interpolation and regularization for pairings) 


	###################################################
	# Output structure
	###################################################

	namestring = "_ph"+str(ph)+"_pl"+str(pl)+"_es"+str(es)+"_fs"+str(fs)


	# -- "Specify paths to image files" --
	# ------------------------------------------------
	print("================================")
	print("Registering ",pair[1]," on ", pair[0])
	print("================================")
	path_ref = fname(pair[0]) #("Reference image :\n")
	path_flo1 = fname(pair[1]) #("First floating image :\n")
	extension = ".inr.gz" # add in
	
	# put here the list of all images to register:
	# -----------------------------------------------------
	# CAUTION (!) : put the reference image at the FIRST place!
	paths=[fname(i) for i in pair]
	outdir = root+"-"+str(pair[1])+"on"+str(pair[0])+namestring
	os.system('mkdir '+outdir)

	# Definition of file-names
	# -------------------------
	# paths to images without extension
	paths_root=[p.split(extension)[0] for p in paths]

	# ------ images :

	# rigid-registered images
	paths_rigid = [outdir+'/'+i +  "_rigid_on_ref.inr" for i in paths_root]

	# affine-registered images
	paths_affine = [outdir+'/'+i +  "_affine_on_ref.inr" for i in paths_root]

	# paths to nonlinearly registered images
	paths_reg = [outdir+'/'+i +  "_registered.inr" for i in paths_root]

	# ------ transformations :

	# optimised (with block-matching) rigid transformations
	rigid_tf = [outdir+'/tr'+str(i)+'-rigid.txt' for i in range(0,len(paths))]

	# optimised (with block-matching) affine transformations
	affine_tf = [outdir+'/tr'+str(i)+'-affine.txt' for i in range(0,len(paths))]

	# non-linear transformations
	nl_tf = [outdir+'/'+i+'_nl.inr' for i in paths_root]

	####################################################################

	ref = fname(pair[0]) #("Reference image :\n")

	# Registration of the floating images on ref
	# -----------------------------------------------------
	
	# A. Find optimised rigid transformations
	for i in range(1,len(paths)) :
		find_rigid_transfo2(ref, paths[i], paths_rigid[i], rigid_tf[i])

	# B. Find optimised affine transformations (initialised by the rigid tfs computed above)
	for i in range(1,len(paths)) :
		find_affine_transfo(ref, paths[i], paths_affine[i], rigid_tf[i], affine_tf[i])

	# C. Find nonlinear transformations which register the affine-transformed images
	for i in range(1,len(paths)) :
		find_nl_transfo(ref, paths_affine[i], paths_reg[i], nl_tf[i] , ph, pl, es, fs)
	
'''
	# D. Apply transformations on the segmented images
	paths_new = [wfname(i) for i in pair]
	apply_tf_seg(paths_new[1:], paths_new[0], affine_tf_list=affine_tf[1:], nl_tf_list=nl_tf[1:], outdir=outdir)
'''
