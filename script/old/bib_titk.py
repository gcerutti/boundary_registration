#!/usr/bin/env python

# This is a library of functions useful for MARS
# it is based on the timagetk python module
# https://mosaic.gitlabpages.inria.fr/timagetk
# 


# general imports :
# -----------------

#import sys
import os
#import time
import numpy as np
from scipy.ndimage.morphology import binary_erosion
#import mahotas
import scipy.ndimage as nd


# timagetk imports :
# ------------------

# io
from timagetk.io import imread, imsave
from timagetk.io import read_trsf, save_trsf
from timagetk.components import SpatialImage

# filtering
from timagetk.plugins import linear_filtering, morphology

# segmentation
from timagetk.plugins import h_transform, region_labeling
from timagetk.algorithms import connexe, regionalext, cell_filter
from timagetk.algorithms import watershed as watershed_titk

# transformations & registering
from timagetk.algorithms.trsf import apply_trsf, inv_trsf, compose_trsf, create_trsf
from timagetk.algorithms.blockmatching import blockmatching
from timagetk.plugins.resampling import isometric_resampling

# registering
#from timagetk.algorithms import compose_trsf, inv_trsf, apply_trsf, create_trsf
#from timagetk.algorithms import blockmatching

# registering and fusing
from timagetk.plugins import registration
from timagetk.plugins import averaging

# ========================
# IO for rigid and affine transformations
# ========================

from timagetk.wrapping.bal_trsf import TRSF_TYPE_DICT, TRSF_UNIT_DICT
from timagetk.algorithms.trsf import allocate_c_bal_matrix

def tfsave(fname, tf):
	matrix = tf.mat.to_np_array().astype(float)
	np.savetxt(fname, matrix, delimiter=";")
	return

def tfread(fname):
	matrix = np.loadtxt(fname, delimiter=";")
	rigid_trsf = create_trsf(param_str_2='-identity', trsf_type=TRSF_TYPE_DICT['RIGID_3D'], trsf_unit=TRSF_UNIT_DICT['REAL_UNIT'])
	allocate_c_bal_matrix(rigid_trsf.mat.c_struct, matrix)
	return rigid_trsf

# ========================
# Segmentation
# ========================

def gauss_titk(img, radius):
	'''
	gaussian filter with radius in real units
	- if radius=a number : isotropic
	- if radius= a list of numbers : anisotropic
	'''
	img_filtered = linear_filtering(img, std_dev=radius, method='gaussian_smoothing', real=True)
	return img_filtered


def filtering(img, filter_type, filter_value):
	if filter_type=='gaussian':
		img_filtered = linear_filtering(img, std_dev=filter_value, method='gaussian_smoothing')
	elif filter_type=='asf'	:	
		img_filtered = img
		if filter_value>0 :
			for i in range(0,filter_value):
				img_filtered = morphology(img_filtered, method='oc_alternate_sequential_filter')
	else:
		print('unknown filter_type...')			
	return img_filtered

def seed_extraction(img,h_minima):
	# local minima
	#reg_min_image = h_transform(img, h=h_minima, method='h_transform_min')
	reg_min_image = h_transform(img, h=h_minima, method='min')
	# labeling of connexe components
	seeds = region_labeling(reg_min_image, low_threshold=1, high_threshold=h_minima, method='connected_components')
	return seeds


def put_background_value(wat_in, label):
	# background is identified as the biggest object in the segmentation
	vols=compute_volumes(wat_in)
	bg_vol = 0
	bg = 1
	for key, val in vols.items():
		if val>bg_vol :
			bg = key
			bg_vol=val
	if bg!=label:
		wat=SpatialImage(wat_in.copy(), voxelsize=wat_in.voxelsize, origin=wat_in.origin )
		if vols.get(label) :
			#swap the two labels, with intermediary label= max+1
			inter = wat.max()+1
			wat[wat==label]=inter
			wat[wat==bg]=label
			wat[wat==inter]=bg
		else :
			wat[wat==bg]=label
	else:
		wat=wat_in
	return wat


def watershed(img, seeds):
	wat = watershed_titk(img, seeds, param_str_1='-l first')
	# put label=1 to the biggest cell (background)
	watnew = put_background_value(wat, 1)
	return watnew


def compute_volumes(segmented):
	"""
	Gives a dictionnary with keys the labels and values the volumes.
	"""
	labels = np.unique(segmented)
	sl=nd.find_objects(segmented)
	volumes={}
	for i in range(0,len(labels)):
		label = labels[i]
		im_slice = segmented[sl[i]].copy()
		coords = np.where(im_slice==label)
		volumes[label]=len(coords[0])
	return volumes


def remove_small_cells (segmented_image, seeds, vol_min=None, real=False, relabel=True) :
	"""
	Seeds corresponding to cells with a volume less then vol_min are removed. 
	:Input:
	- `segmented_image` (|SpatialImage|) - segmented image
	- `seeds` (|SpatialImage|) - labeled markers image
	- `vol_min` - minimum volume
	- `real` (bool, optional) - If True, volume is in real-world units else in voxels (default)
	- `relabel' (bool, optional) - If True, the seeds are relabeled so that removed seeds do not cause "holes" in the labeling
	:Output:
	- the new seed image
	"""
	volumes=compute_volumes(segmented_image)
	# do not modify the background (labeled by 1)
	volumes.pop(1)
	new_seeds=SpatialImage(seeds.copy(), voxelsize=seeds.voxelsize, origin=seeds.origin )
	if vol_min==None:
		vol_min=np.uint(np.mean(volumes)-2*np.std(volumes))
	for label, vol in volumes.items():
		if vol < vol_min:
			print("Remove cell number ",label,", volume ",volumes[label])
			new_seeds[new_seeds==label]=0
	if relabel==True:
		regions = SpatialImage(new_seeds>0, voxelsize=segmented_image.voxelsize, origin=segmented_image.origin, dtype='uint8')
		# relabeling of connexe components
		new_seeds = region_labeling(regions, low_threshold=1, high_threshold=1, method='connected_components')
	return new_seeds


def segmentation_watershed(img_seeds, img_wat, h_minima, voxel_volmin) :
	#searching the seeds 
	seeds = seed_extraction(img_seeds,h_minima)
	# first watershed
	wat = watershed(img_wat, seeds)
	#removing small cells
	new_seeds = remove_small_cells (wat,seeds,voxel_volmin)
	# new watershed
	wat = watershed(img_wat, new_seeds)
	return wat, new_seeds


def most_frequent_below_tissuesurface(img, outspace):
	# estimating most frequent intensity value inside the tissue, near the surface
	inspace = complementary(outspace)
	inspace_top = (nd.binary_erosion(inspace,iterations=1)).astype('uint8')
	inspace_bottom = (nd.binary_erosion(inspace_top,iterations=10)).astype('uint8')
	estimation_mask = inspace_top-inspace_bottom
	coords = np.where(estimation_mask==1)
	zone_values = [ img[coords[0][i],coords[1][i],coords[2][i]]  for i in range(0,len(coords[0]))]
	return np.argmax(np.bincount(zone_values))

# ========================
# Registration
# ========================


def find_rigid_transfo2(path_ref, path_flo, path_res, path_rigid):
	print("rigid registration... result saved in :", path_res)
	im_ref=imread(path_ref)
	im_flo=imread(path_flo)
	parameters=("-estimator wlts" +	
		" -floating-low-threshold 0" +
		" -reference-low-threshold 0" +
		" -pyramid-highest-level 5" +
		" -pyramid-lowest-level 2" +
		" -lts-fraction 0.55")
	trsf_out,im_res=blockmatching(im_flo, im_ref, param_str_2=parameters)
	imsave(path_res, im_res)
	#save_trsf(trsf_out, path_rigid)
	tfsave(path_rigid,trsf_out)
	return


def find_affine_transfo(path_ref, path_flo, path_res, trf_init, path_affine):
	print("rigid registration... result saved in :", path_res)
	im_ref=imread(path_ref)
	im_flo=imread(path_flo)
	init_tf=tfread(trf_init)
	#init_tf=read_trsf( trf_init)
	tftype=" -trsf-type affine"
	parameters=("-estimator wlts" +	
		" -floating-low-threshold 0" +
		" -reference-low-threshold 0" +
		" -pyramid-highest-level 3" +
		" -pyramid-lowest-level 2" +
		" -lts-fraction 0.55")
	trsf_out,im_res=blockmatching(im_flo, im_ref, init_result_trsf=init_tf,
		param_str_1=tftype, param_str_2=parameters)
	imsave(path_res, im_res)
	tfsave(path_affine, trsf_out)
	#save_trsf(trsf_out, path_affine)
	return


def find_nl_transfo(path_ref, path_flo, path_res, tf_name, ph=5, pl=2, es=3.0, fs=3.0):
	bs = 5 # by default = 4
	print("nonlinear registration... result saved in :", path_res)
	im_ref=imread(path_ref)
	im_flo=imread(path_flo)
	tftype=" -trsf-type vectorfield"
	parameters=(
		" -estimator wlts" +
		" -py-gf" +
		" -floating-low-threshold 2" +
		" -reference-low-threshold 2" +
		#" -reference-removed-fraction  0.25" +
		#" -block-size " + str(bs) + " " + str(bs) + " " + str(bs) +
		" -pyramid-highest-level "+str(ph) +
		" -pyramid-lowest-level "+str(pl) +
		" -elastic-sigma "+str(es)+
		" -fluid-sigma "+str(fs))
	trsf_out,im_res=blockmatching(im_flo, im_ref,
		param_str_1=tftype, param_str_2=parameters)
	imsave(path_res, im_res)
	save_trsf(trsf_out, tf_name)
	return


def apply_transfo_on_seg(transfo, path_in, path_out, ref):
	im_in=imread(path_in)
	im_ref=imread(ref)
	tf=trsf_read(transfo)
	im_out=apply_trsf(im_in, trsf=tf, template_img=im_ref, param_str_2="-nearest")
	imsave(path_out, im_out)
	return


def isotropic_resampling_seg(path_in, path_out, isores):
	im_in=imread(path_in)
	im_out=apply_trsf(im_in, param_str_2="-nearest"+" -iso "+str(isores))
	imsave(path_out, im_out)
	return

def isotropic_resampling(path_in, path_out, isores):
	im_in=imread(path_in)
	im_out=apply_trsf(im_in, param_str_2=" -iso "+str(isores))
	imsave(path_out, im_out)
	return


def apply_tf_seg(path_list, template, affine_tf_list=[], nl_tf_list=[],outdir='') :
	'''
	apply transformations on the path_list images
	path_list : list of paths to the images
	template : reference image, useful if resampling has to be performed
	affine_tf_list : list of affine (or rigid) transformations to apply to the masks
	nl_tf_list : list of nonlinear transformations to apply to the masks 
	'''
	if outdir!='' :
		dirname=outdir+'/'
	# transform images
	temp_list = [dirname+'registered-'+i for i in path_list]
	for i in range(0,len(path_list)):
		os.system('scp '+path_list[i]+' '+temp_list[i])
	for i in range(0,len(path_list)):
		if len(affine_tf_list) == len(path_list) :
			if affine_tf_list[i] != "" :
				apply_transfo_on_seg(affine_tf_list[i], temp_list[i], temp_list[i], template)
		if len(nl_tf_list) == len(path_list) :
			if nl_tf_list[i] != "" :
				apply_transfo_on_seg(nl_tf_list[i], temp_list[i], temp_list[i], template)
	return 1




def isotropic_resampling(path_in, path_out, isores):
    os.system(pathAT +
          " "+path_in +
          " "+path_out +
          " -iso "+str(isores) ) # isotropic voxelsize


	
