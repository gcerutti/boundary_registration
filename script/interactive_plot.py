#!/usr/bin/env python

import os
import argparse
import logging

import numpy as np
import matplotlib.pyplot as plt

from titk_tools.io import imread, imsave
from titk_tools.segmentation import filtering, segmentation_watershed
from titk_tools.visualization import plot_image, plot_image_surface_segmentation
from titk_tools.visualization import plot_image_slice, plot_segmentation_slice

from titk_tools.interactive_visualization.orthogonal_view_plot import OrthogonalViewPlot
from titk_tools.interactive_visualization.surface_slice_plot import SurfaceSlicePlot

logging.getLogger().setLevel(logging.INFO)

orientation_choices = ['upright', 'inverted', 'none']
plot_choices = ['ortho', 'slice']

parser = argparse.ArgumentParser()
parser.add_argument('image_filename', help='path to the original microscopy image')
parser.add_argument('-s', '--segmentation_filename', help='path to the corresponding segmented image', default=None)
parser.add_argument('-p', '--plot-type', help='type of interactive plot to display', type=str, default='slice', choices=plot_choices)
parser.add_argument('-o', '--microscope-orientation', help='whether the image comes from an upright or inverted microscope', type=str, default='none', choices=orientation_choices)
args = parser.parse_args()

#############################
# filename definitions :
#############################
image_filename = args.image_filename
segmentation_filename = args.segmentation_filename

plot_type = args.plot_type

orientation_dict = {'upright':1, 'inverted':-1, 'none':None}
orientation = orientation_dict[args.microscope_orientation]

img = imread(image_filename)
if segmentation_filename is not None:
    seg_img = imread(segmentation_filename)
else:
    seg_img = None


figure = plt.figure(0)
figure.clf()


if args.plot_type == 'ortho':
    view = OrthogonalViewPlot(figure, img, seg_img=seg_img, colormap='Greys', value_range=(0, 255))
elif args.plot_type == 'slice':
    view = SurfaceSlicePlot(figure, img, seg_img=seg_img, orientation=orientation, colormap='Greys', value_range=(0, 255))

plt.show(block=True)
