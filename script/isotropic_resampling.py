#!/usr/bin/env python

import os
import argparse
import logging

import numpy as np
import matplotlib.pyplot as plt


from titk_tools.registration import isotropic_resampling
from titk_tools.io import imread, imsave

logging.getLogger().setLevel(logging.INFO)

sampling_choices = ['min', 'max', 'avg', 'voxelsize']
#sampling_choices = ['min', 'max', 'avg']

parser = argparse.ArgumentParser()
parser.add_argument('image_filename', help='path to the image to reasmple')
parser.add_argument('-isotype', '--sampling-type', help='the choice of the isotropic voxelsize (mini, maxi, avg, voxelsize)', type=str, default="avg", choices=sampling_choices)
parser.add_argument('-isovox', '--isotropic-voxelsize', help='the isotropic voxelsize (in µm)', type=float, default=None)
parser.add_argument('-zreverse', '--z-reverse', help='whether to reverse the image in z', type=int, default=0, choices=[0, 1])


args = parser.parse_args()

imname = args.image_filename
isotype = args.sampling_type
isores = args.isotropic_voxelsize
zreverse = args.z_reverse

#############################
# main :
#############################

im=imread(imname)
s=im.voxelsize

if zreverse :
	im[:,:,:]=im[:,:,::-1]

imnameroot = imname.split('.')[0]
path_out=imnameroot+"_iso.inr.gz"

if isotype=='min':
	isores = min(s)

elif isotype=='max' :
	isores = max(s)

elif isotype=='avg' :
	isores=(s[0]*s[1]*s[2])**(1/3.)

print("Old voxelsize :", im.voxelsize)
im_out = isotropic_resampling(im, isores)
print("New voxelsize :", im_out.voxelsize)
imsave(path_out, im_out)

