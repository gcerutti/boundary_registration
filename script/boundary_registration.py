import os
import argparse
import time
import logging

import matplotlib.pyplot as plt

from titk_tools.io import imread, imsave, tfread, tfsave, save_trsf
from titk_tools.registration import find_rigid_transfo2, find_affine_transfo, find_nl_transfo
from titk_tools.visualization import plot_image_rgb_blend

logging.getLogger().setLevel(logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('data_dirname', help='path to the image data to process')
parser.add_argument('-s', '--save-files', help='whether to save intermediary files', default=False, action='store_true')
parser.add_argument('-p', '--plot-images', help='whether to plot registered images', default=True, action='store_false')
parser.add_argument('-o', '--microscope-orientation', help='whether the image comes from an upright or inverted microscope', type=str, default='none', choices=orientation_choices)
parser.add_argument('-ph', '--pyramid-highest-level', help='pyramid highest level (3 corresponds to 32x32x32 for an original 256x256x256 image)', type=int, default=6)
parser.add_argument('-pl', '--pyramid-lowest-level', help='pyramid lowest level (0 corresponds to original image dimension)', type=int, default=1)
parser.add_argument('-es', '--elastic-sigma', help='sigma for elastic regularization of the transformation', type=int, default=5)
parser.add_argument('-fs', '--fluid-sigma', help='sigma for fluid regularization (field interpolation and regularization for pairings)', type=int, default=3)
args = parser.parse_args()

start = time.time()

dirname = args.data_dirname
save_files = args.save_files
plot_images = args.plot_images

orientation_dict = {'upright':1, 'inverted':-1, 'none':None}
orientation = orientation_dict[args.microscope_orientation]

ph = args.pyramid_highest_level
pl = args.pyramid_lowest_level
es = args.elastic_sigma
fs = args.fluid_sigma

###################################################
# Input data:
###################################################

# filenames
sequence_name = "2013-03-05_LTi6B_dis_A"

# times to register
file_times = [0, 12]
#file_times = [0, 12, 24, 36, 48]

filenames = [sequence_name + "_T" + str(t).zfill(2) + 'h' for t in file_times]

# -- "Specify paths to image files" --
# ------------------------------------------------
images = {}
for filename in filenames:
	image_filename = dirname + '/' + filename + ".tif"
	images[filename] = imread(image_filename)

	figure = plt.figure(1)
	figure.clf()

	plot_image_rgb_blend(figure, green_img=images[filename], projection='surface', orientation=orientation)

	figure.set_size_inches(10, 10)
	figure.tight_layout()
	figure_filename = dirname + '/' + filename + "_signal.png"
	figure.savefig(figure_filename)

rigid_images = {}
rigid_transformations = {}
affine_images = {}
affine_transformations = {}
registered_images = {}
non_linear_transformations = {}
# if we want to register (i+1) on (i)
# for reference_filename, floating_filename, reference_time, floating_time, in zip(filenames[:-1],filenames[1:],time_labels[:-1],time_labels[1:]):
# if we want to register (i) on (i+1)	
for reference_filename, floating_filename, reference_time, floating_time, in zip(filenames[1:],filenames[:-1],file_times[1:],file_times[:-1]):

	print("================================")
	print("Registering ", floating_time, " on ", reference_time)
	print("================================")

	###################################################
	# Output structure
	###################################################

	namestring = "_ph"+str(ph)+"_pl"+str(pl)+"_es"+str(es)+"_fs"+str(fs)

	output_dirname = dirname + '/' + sequence_name + "_T" + str(floating_time).zfill(2) + "h_on_T" + str(reference_time).zfill(2) + "h" + namestring
	if not os.path.exists(output_dirname):
		os.makedirs(output_dirname)

	reference_img = images[reference_filename]
	floating_img = images[floating_filename]

	####################################################################

	# Registration of the floating images on ref
	# -----------------------------------------------------

	# A. Find optimised rigid transformations
	rigid_img, rigid_trsf = find_rigid_transfo2(reference_img, floating_img)
	rigid_images[floating_filename] = rigid_img
	rigid_transformations[floating_filename] = rigid_trsf
	
	if save_files:
		# rigid-registered images
		rigid_filename = output_dirname + '/' + floating_filename + "_rigid_on_T" + str(reference_time).zfill(2) + "h.inr.gz"
		imsave(rigid_filename, rigid_img)

		# optimised (with block-matching) rigid transformations
		rigid_trsf_filename = output_dirname + '/tr_' + floating_filename + '_rigid.txt'
		tfsave(rigid_trsf_filename, rigid_trsf)

	if plot_images:
		figure = plt.figure(0)
		figure.clf()

		figure.add_subplot(1, 3, 1)
		plot_image_rgb_blend(figure, red_img=rigid_img, green_img=reference_img, projection='surface', orientation=orientation)
		figure.gca().set_title("Rigid registration", size=20)

	# B. Find optimised affine transformations (initialised by the rigid tfs computed above)
	affine_img, affine_trsf = find_affine_transfo(reference_img, floating_img, init_trsf=rigid_trsf)
	affine_images[floating_filename] = affine_img
	affine_transformations[floating_filename] = affine_trsf

	if save_files:
		# affine-registered images
		affine_filename = output_dirname + '/' + floating_filename + "_affine_on_T" + str(reference_time).zfill(2) + "h.inr.gz"
		imsave(affine_filename, affine_img)

		# optimised (with block-matching) affine transformations
		affine_trsf_filename = output_dirname + '/tr_' + floating_filename + '_affine.txt'
		tfsave(affine_filename, affine_trsf)

	if plot_images:
		figure.add_subplot(1, 3, 2)
		plot_image_rgb_blend(figure, red_img=affine_img, green_img=reference_img, projection='surface', orientation=orientation)
		figure.gca().set_title("Affine registration", size=20)

	# C. Find nonlinear transformations which register the affine-transformed images
	registered_img, nl_trsf = find_nl_transfo(reference_img, affine_img, ph=ph, pl=pl, es=es, fs=fs)
	registered_images[floating_filename] = registered_img
	non_linear_transformations[floating_filename] = nl_trsf

	# paths to nonlinearly registered images
	registered_filename = output_dirname + '/' + floating_filename + "_registered_on_T" + str(reference_time).zfill(2) + "h.inr.gz"
	imsave(registered_filename, registered_img)

	if save_files:
		# non-linear transformations
		nl_trsf_filename = output_dirname + '/' + floating_filename + '_vectorfield.inr.gz'
		save_trsf(nl_trsf, nl_trsf_filename, compress=True)

	if plot_images:
		figure.add_subplot(1, 3, 3)
		plot_image_rgb_blend(figure, red_img=registered_img, green_img=reference_img, projection='surface', orientation=orientation)
		figure.gca().set_title("Non-linear registration", size=20)

		figure.set_size_inches(30, 10)
		figure.tight_layout(rect=[0,0,1,0.98])
		figure_filename = output_dirname + '/' + floating_filename + "_registration_on_T" + str(reference_time).zfill(2) + "h.png"
		figure.savefig(figure_filename)
