#!/usr/bin/env python

import os
import argparse
import logging
from time import time as current_time

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.colorbar import ColorbarBase
from matplotlib.colors import Normalize
from matplotlib.cm import get_cmap

from visu_core.vtk.display import vtk_image_actors
from visu_core.vtk.actor import vtk_actor
from visu_core.vtk.utils.image_tools import vtk_image_data_from_image, image_to_vtk_cell_polydatas
from visu_core.vtk.utils.polydata_tools import vtk_combine_polydatas, vtk_slice_polydata
from visu_core.matplotlib.colormap import plain_colormap

from titk_tools.io import imread, imsave
from titk_tools.segmentation import is_segmented, SpatialImage
from titk_tools.quantification import LabelledImage, compute_centers, compute_volumes, compute_surfel_neighbors, compute_layers
from titk_tools.geometry import compute_surface_curvature
from titk_tools.visualization import glasbey, plot_image_surface_segmentation

from cellcomplex.utils import array_dict

logging.getLogger().setLevel(logging.INFO)

orientation_choices = ['upright', 'inverted', 'none']

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('segmentation_filename', help='path to the segmented image on which the properties have been computed')
parser.add_argument('-p', '--property-name', help='which cell property to plot', type=str, default='volume')
parser.add_argument('-o', '--microscope-orientation', help='whether the image comes from an upright or inverted microscope', type=str, default='none', choices=orientation_choices)
parser.add_argument('-v', '--minimum-value', help='lower bound of cell property heatmap', type=float, default=None)
parser.add_argument('-V', '--maximum-value', help='upper bound of cell property heatmap', type=float, default=None)
parser.add_argument('-c', '--colormap', help='matplotlib colormap to apply to cell volume heatmap', type=str, default='plasma')
parser.add_argument('-f', '--figure-size', help='size of the figure to save', type=float, default=10.)
parser.add_argument('-nc', '--nan-value-color', help='matplotlib color to render cells with no property', type=str, default='k')
parser.add_argument('-no', '--nan-value-opacity', help='opacity to render cells with no property', type=float, default=1)
parser.add_argument('-r', '--rotated-views', help='whether to generate rotated views of the image', action='store_true')

args = parser.parse_args()

#############################
# filename definitions :
#############################

segmentation_filename = args.segmentation_filename

orientation_dict = {'upright':1, 'inverted':-1, 'none':None}
orientation = orientation_dict[args.microscope_orientation]

filename = os.path.splitext(segmentation_filename.split('/')[-1])[0]
if segmentation_filename.endswith('.inr.gz'):
    filename = os.path.splitext(filename)[0]
if filename.endswith('_wat'):
    filename = filename[:-4]
if '/' in segmentation_filename:
    dirname = os.path.join(*segmentation_filename.split('/')[:-1])
else:
    dirname = '.'
if segmentation_filename[0] == '/':
    dirname = '/' + dirname

value_range = [args.minimum_value, args.maximum_value]

colormap = args.colormap
try:
    get_cmap(colormap)
except ValueError:
    colormap = 'plasma'

nan_color = args.nan_value_color
nan_opacity = args.nan_value_opacity

#############################
# main :
#############################

seg_img = imread(segmentation_filename)

assert is_segmented(seg_img.get_array())
seg_img = LabelledImage(seg_img, no_label_id=0)

if orientation is None:
    orientation = 1 - 2*(np.mean(seg_img[:,:,0]) > np.mean(seg_img[:,:,-1]))

# cell_center = compute_centers(seg_img, real=True)
# cell_volume = compute_volumes(seg_img, real=True)
#
# cell_neighbors = compute_surfel_neighbors(seg_img)
# cell_layer = compute_layers(seg_img, cell_neighbors=cell_neighbors)
# cell_curvature = compute_surface_curvature(seg_img, orientation=orientation, cell_neighbors=cell_neighbors)
#
# cell_df = pd.DataFrame({'label':list(cell_layer.keys())})
# for k,dim in enumerate('xyz'):
#     cell_df['center_'+dim] = [cell_center[c][k] for c in cell_df['label'].values]
# cell_df['volume'] = [cell_volume[c] for c in cell_df['label'].values]
# cell_df['layer'] = [cell_layer[c] for c in cell_df['label'].values]
# for curvature_name in cell_curvature.keys():
#     cell_df[curvature_name] = [cell_curvature[curvature_name][c] for c in cell_df['label'].values]

data_filename = dirname + '/' + filename + "_cell_data.csv"
cell_df = pd.read_csv(data_filename)
if not 'label' in cell_df.columns:
    cell_df = pd.read_csv(data_filename, delimiter=';')

cell_center = dict(zip(cell_df['label'].values, cell_df[['center_'+dim for dim in 'xyz']].values))
cell_layer = dict(zip(cell_df['label'].values, cell_df['layer'].values))

if args.property_name not in cell_df.columns:
    raise NameError(f"The property {args.property_name} is not computed on {filename}! Nothing to plot!")

cell_property = dict(zip(cell_df['label'].values, cell_df[args.property_name].values))
if value_range[0] is None:
    value_range[0] = np.nanmin(cell_df[args.property_name].values)
if value_range[1] is None:
    value_range[1] = np.nanmax(cell_df[args.property_name].values)

labels = [l for l in seg_img.labels() if l!=1]
x_labels = [c for c in labels if orientation*cell_center[c][0] > orientation*(0.5-0.02*orientation)*seg_img.extent[0]]
y_labels = [c for c in labels if orientation*cell_center[c][1] < orientation*(0.5+0.02*orientation)*seg_img.extent[1]]

l1_labels = [c for c in labels if cell_layer[c] == 1]
l2_labels = [c for c in labels if cell_layer[c] == 2]
l3_labels = [c for c in labels if cell_layer[c] == 3]
l4_labels = [c for c in labels if cell_layer[c] == 4]

if orientation is None:
    orientation = 1 - 2 * (np.mean(seg_img[:, :, 0]) > np.mean(seg_img[:, :, -1]))

cell_polydatas = image_to_vtk_cell_polydatas(seg_img,
                                             labels=labels,
                                             subsampling=[int(np.round(0.6/v)) for v in seg_img.voxelsize],
                                             smoothing=10)

figure = plt.figure(0)
figure.clf()

figure.add_subplot(4, 4, 1)

actors = []
cell_polydata = vtk_combine_polydatas([cell_polydatas[l] for l in labels])
cell_actor = vtk_actor(cell_polydata, colormap='glasbey', value_range=(0,255))
actors += [cell_actor]

top_view = vtk_image_actors(actors, focal_point=(0,0,orientation), view_up=(0,-orientation,0))
figure.gca().imshow(top_view[::-1])
figure.gca().axis('off')
figure.gca().set_title("Image Segmentation", size=20)

figure.add_subplot(4, 4, 2)

plot_image_surface_segmentation(figure, orientation=orientation, seg_img=seg_img,
                                plot_type='image', plot_labels=True, alpha=1,
                                colormap='glasbey', value_range=(0,255))

figure.gca().set_xlim(0,seg_img.extent[0])
figure.gca().set_ylim(0,seg_img.extent[1])
figure.gca().axis('off')
figure.gca().set_title("Image Segmentation Surface", size=20)

figure.add_subplot(4, 4, 3)

actors = []
x_polydata = vtk_combine_polydatas([cell_polydatas[l] for l in x_labels])
x_actor = vtk_actor(x_polydata, colormap='glasbey', value_range=(0,255))
actors += [x_actor]

x_view = vtk_image_actors(actors, focal_point=(orientation,0,0),view_up=(0,0,-orientation))
figure.gca().imshow(x_view[::-1])
figure.gca().axis('off')
figure.gca().set_title("YZ Image Segmentation", size=20)

figure.add_subplot(4, 4, 4)

actors = []
y_polydata = vtk_combine_polydatas([cell_polydatas[l] for l in y_labels])
y_actor = vtk_actor(y_polydata, colormap='glasbey', value_range=(0,255))
actors += [y_actor]

y_view = vtk_image_actors(actors, focal_point=(0,-orientation,0), view_up=(0,0,-orientation))
figure.gca().imshow(y_view[::-1])
figure.gca().axis('off')
figure.gca().set_title("XZ Image Segmentation", size=20)

layer_polydatas = image_to_vtk_cell_polydatas(seg_img,
                                              labels=labels,
                                              cell_property=cell_layer,
                                              cell_polydatas=cell_polydatas)


figure.add_subplot(4, 4, 5)

actors = []
x_polydata = vtk_combine_polydatas([layer_polydatas[l] for l in x_labels])
x_actor = vtk_actor(x_polydata, colormap='jet', value_range=(0,4))
actors += [x_actor]

x_view = vtk_image_actors(actors, focal_point=(orientation,0,0),view_up=(0,0,-orientation))
figure.gca().imshow(x_view[::-1])
figure.gca().axis('off')

figure.add_subplot(4, 4, 6)

actors = []
y_polydata = vtk_combine_polydatas([layer_polydatas[l] for l in y_labels])
y_actor = vtk_actor(y_polydata, colormap='jet', value_range=(0,4))
actors += [y_actor]

y_view = vtk_image_actors(actors, focal_point=(0,-orientation,0), view_up=(0,0,-orientation))
figure.gca().imshow(y_view[::-1])
figure.gca().axis('off')

property_polydatas = image_to_vtk_cell_polydatas(seg_img,
                                                 labels=labels,
                                                 cell_property=cell_property,
                                                 cell_polydatas=cell_polydatas)

figure.add_subplot(4, 4, 7)

actors = []
property_x_labels = [l for l in x_labels if not np.isnan(cell_property[l])]
no_property_x_labels = [l for l in x_labels if l not in property_x_labels]

x_polydata = vtk_combine_polydatas([property_polydatas[l] for l in property_x_labels])
x_actor = vtk_actor(x_polydata, colormap=colormap, value_range=value_range)
actors += [x_actor]

x_polydata = vtk_combine_polydatas([cell_polydatas[l] for l in no_property_x_labels])
x_actor = vtk_actor(x_polydata, colormap=plain_colormap(nan_color), opacity=nan_opacity)
actors += [x_actor]

x_view = vtk_image_actors(actors, focal_point=(orientation,0,0),view_up=(0,0,-orientation))
figure.gca().imshow(x_view[::-1])
figure.gca().axis('off')

figure.add_subplot(4, 4, 8)

actors = []
property_y_labels = [l for l in y_labels if not np.isnan(cell_property[l])]
no_property_y_labels = [l for l in y_labels if l not in property_y_labels]

y_polydata = vtk_combine_polydatas([property_polydatas[l] for l in property_y_labels])
y_actor = vtk_actor(y_polydata, colormap=colormap, value_range=value_range)
actors += [y_actor]

y_polydata = vtk_combine_polydatas([cell_polydatas[l] for l in no_property_y_labels])
y_actor = vtk_actor(y_polydata, colormap=plain_colormap(nan_color), opacity=nan_opacity)
actors += [y_actor]

y_view = vtk_image_actors(actors, focal_point=(0,-orientation,0), view_up=(0,0,-orientation))
figure.gca().imshow(y_view[::-1])
figure.gca().axis('off')

for layer, layer_labels in zip([1,2,3,4],[l1_labels,l2_labels,l3_labels,l4_labels]):

    figure.add_subplot(4, 4, 8+layer)

    actors = []
    cell_polydata = vtk_combine_polydatas([property_polydatas[l] for l in labels])
    cell_actor = vtk_actor(cell_polydata, opacity=0)
    actors += [cell_actor]

    property_layer_labels = [l for l in layer_labels if not np.isnan(cell_property[l])]
    no_property_layer_labels = [l for l in layer_labels if l not in property_layer_labels]

    cell_polydata = vtk_combine_polydatas([property_polydatas[l] for l in property_layer_labels])
    cell_actor = vtk_actor(cell_polydata, colormap=colormap, value_range=value_range)
    actors += [cell_actor]

    cell_polydata = vtk_combine_polydatas([cell_polydatas[l] for l in no_property_layer_labels])
    cell_actor = vtk_actor(cell_polydata, colormap=plain_colormap(nan_color), opacity=nan_opacity)
    actors += [cell_actor]

    top_view = vtk_image_actors(actors, focal_point=(0,0,orientation), view_up=(0,-orientation,0))
    figure.gca().imshow(top_view[::-1])
    figure.gca().axis('off')


cell_layer[1] = 0
all_layer_img = LabelledImage(SpatialImage(array_dict(cell_layer).values(seg_img.get_array()), voxelsize=seg_img.voxelsize), no_label_id=0)

for layer,layer_labels in zip([1,2,3,4],[l1_labels,l2_labels,l3_labels,l4_labels]):

    layer_seg_img = seg_img.copy()
    layer_seg_img[all_layer_img < layer] = 1

    figure.add_subplot(4, 4, 12+layer)

    plot_image_surface_segmentation(figure, orientation=orientation, seg_img=layer_seg_img,
                                    plot_type='image', plot_labels=True, alpha=1,
                                    cell_property=cell_property, colormap=colormap, value_range=value_range)

    figure.gca().set_xlim(0,seg_img.extent[0])
    figure.gca().set_ylim(0,seg_img.extent[1])
    figure.gca().axis('off')

cax = figure.gca().inset_axes([0.95, 0.05, 0.03, 0.33])
cbar = ColorbarBase(cax, orientation='vertical', cmap=get_cmap(colormap),
                   norm=Normalize(vmin=value_range[0], vmax=value_range[1]))
cax.yaxis.set_ticks_position('left')
cbar.set_label(f'{args.property_name}',size=16)


figure.set_size_inches(4*args.figure_size, 4*args.figure_size)
figure.tight_layout()
figure_filename = dirname + '/' + filename + f"_segmentation_3d_layer_{args.property_name}s.png"
figure.savefig(figure_filename)

if args.rotated_views:
    figure = plt.figure(1)
    figure.clf()

    angle_range = np.arange(8)*45
    h = 45

    for i_a, a in enumerate(angle_range):
        figure.add_subplot(2, 4, i_a+1)

        actors = []
        cell_polydata = vtk_combine_polydatas([property_polydatas[l] for l in labels])
        cell_actor = vtk_actor(cell_polydata, opacity=0)
        actors += [cell_actor]

        property_labels = [l for l in labels if not np.isnan(cell_property[l])]
        no_property_labels = [l for l in labels if l not in property_labels]

        cell_polydata = vtk_combine_polydatas([property_polydatas[l] for l in property_labels])
        cell_actor = vtk_actor(cell_polydata, colormap=colormap, value_range=value_range)
        actors += [cell_actor]

        cell_polydata = vtk_combine_polydatas([cell_polydatas[l] for l in no_property_labels])
        cell_actor = vtk_actor(cell_polydata, colormap=plain_colormap(nan_color), opacity=nan_opacity)
        actors += [cell_actor]

        focal_point = (
            np.cos(np.radians(h)) * np.cos(np.radians(a)),
            np.cos(np.radians(h)) * np.sin(np.radians(a)),
            np.sin(np.radians(h)) * orientation
        )

        rotated_view = vtk_image_actors(actors, focal_point=focal_point, view_up=(0,0,-orientation))
        figure.gca().imshow(rotated_view[::-1])
        figure.gca().axis('off')

    figure.set_size_inches(4*args.figure_size, 2*args.figure_size)
    figure.tight_layout()
    figure_filename = dirname + '/' + filename + f"_segmentation_3d_rotated_{args.property_name}s.png"
    figure.savefig(figure_filename)