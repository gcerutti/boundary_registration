#!/usr/bin/env python

import os
import argparse
import logging
from time import time as current_time

import numpy as np
import scipy.ndimage as nd

import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.colorbar import ColorbarBase
from matplotlib.colors import Normalize
from matplotlib.cm import get_cmap
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


from titk_tools.io import imread, imsave
from titk_tools.segmentation import is_segmented, SpatialImage
from titk_tools.quantification import LabelledImage, compute_centers, compute_volumes, compute_surfaces, compute_diameters, compute_surfel_neighbors, compute_layers
from titk_tools.geometry import compute_surface_curvature

from cellcomplex.utils import array_dict

logging.getLogger().setLevel(logging.INFO)

orientation_choices = ['upright', 'inverted', 'none']

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('segmentation_filename', help='path to the segmented image')
parser.add_argument('-sig', '--signal-image-filename', help='path to the signal image', type=str, default='None')
parser.add_argument('-o', '--microscope-orientation', help='whether the image comes from an upright or inverted microscope', type=str, default='none', choices=orientation_choices)
#parser.add_argument('-v', '--minimum-volume', help='lower bound of cell volume heatmap (in µm3)', type=float, default=80.)
#parser.add_argument('-V', '--maximum-volume', help='upper bound of cell volume heatmap (in µm3)', type=float, default=400.)


args = parser.parse_args()

#############################
# filename definitions :
#############################

segmentation_filename = args.segmentation_filename
signal_filename = args.signal_image_filename


orientation_dict = {'upright':1, 'inverted':-1, 'none':None}
orientation = orientation_dict[args.microscope_orientation]

filename = os.path.splitext(segmentation_filename.split('/')[-1])[0]
if segmentation_filename.endswith('.inr.gz'):
    filename = os.path.splitext(filename)[0]
if filename.endswith('_wat'):
    filename = filename[:-4]
if '/' in segmentation_filename:
    dirname = os.path.join(*segmentation_filename.split('/')[:-1])
else:
    dirname = '.'
if segmentation_filename[0] == '/':
    dirname = '/' + dirname

#volume_range = (args.minimum_volume, args.maximum_volume)

#############################
# main :
#############################

# computing cell properties
print('----- computing cell properties ------')
seg_img = imread(segmentation_filename)
s=seg_img.voxelsize
voxelvolume=s[0]*s[1]*s[2]

assert is_segmented(seg_img.get_array())
seg_img = LabelledImage(seg_img, no_label_id=0)

if orientation is None:
    orientation = 1 - 2*(np.mean(seg_img[:,:,0]) > np.mean(seg_img[:,:,-1]))

cell_center = compute_centers(seg_img, real=True)
cell_volume = compute_volumes(seg_img, real=True)
cell_surface = compute_surfaces(seg_img, real=True)
cell_Dmax = compute_diameters(seg_img, real=True)

cell_neighbors = compute_surfel_neighbors(seg_img)
cell_layer = compute_layers(seg_img, cell_neighbors=cell_neighbors)
cell_curvature = compute_surface_curvature(seg_img, orientation=orientation, cell_neighbors=cell_neighbors)


# organise data in a dataframe
cell_df = pd.DataFrame({'label':list(cell_layer.keys())})
for k,dim in enumerate('xyz'):
    cell_df['center_'+dim] = [cell_center[c][k] for c in cell_df['label'].values]
cell_df['volume'] = [cell_volume[c] for c in cell_df['label'].values]
cell_df['surface'] = [cell_surface[c] for c in cell_df['label'].values]
cell_df['sphericity'] = [36*np.pi*cell_volume[c]**2/cell_surface[c]**3 for c in cell_df['label'].values]
cell_df['Dmax'] = [cell_Dmax[c] for c in cell_df['label'].values]
cell_df['layer'] = [cell_layer[c] for c in cell_df['label'].values]
for curvature_name in cell_curvature.keys():
    cell_df[curvature_name] = [cell_curvature[curvature_name][c] for c in cell_df['label'].values]


# computing signal properties
def mean_signal(seg, sign):
    """
    compute the mean signal value (from sign) 
    per region of a segmented image (seg
    """
    r={}
    l=list(np.unique(seg))
    if 1 in l:l.remove(1)
    o=nd.measurements.mean(sign,seg,l)
    return dict(zip(l,o))

signal_img = imread(signal_filename)
values=mean_signal(seg_img, signal_img)


cell_df['MeanSignal']=[values[c] for c in cell_df['label'].values]
cell_df['TotalSignal']=cell_df['MeanSignal']*cell_df['volume']/voxelvolume

# writing results into textfile  
data_filename = dirname + '/' + filename + "_analysis.csv"
cell_df.to_csv(data_filename,index=False)


