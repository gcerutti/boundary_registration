#!/usr/bin/env python

import os
import argparse
import logging
from time import time as current_time

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.colorbar import ColorbarBase
from matplotlib.colors import Normalize
from matplotlib.cm import get_cmap

from visu_core.vtk.display import vtk_image_actors
from visu_core.vtk.actor import vtk_actor
from visu_core.vtk.utils.image_tools import vtk_image_data_from_image, image_to_vtk_cell_polydatas
from visu_core.vtk.utils.polydata_tools import vtk_combine_polydatas, vtk_slice_polydata
from visu_core.matplotlib.colormap import plain_colormap

from titk_tools.io import imread, imsave
from titk_tools.segmentation import is_segmented, SpatialImage
from titk_tools.quantification import LabelledImage, compute_centers, compute_volumes, compute_surfel_neighbors, compute_layers
from titk_tools.geometry import compute_surface_curvature, _segmentation_surface_mesh, curvature_properties
from titk_tools.visualization import glasbey, plot_image_surface_segmentation

from cellcomplex.utils import array_dict
from cellcomplex.property_topomesh.io import save_ply_property_topomesh
from cellcomplex.property_topomesh.visualization.vtk_actor_topomesh import VtkActorTopomesh

logging.getLogger().setLevel(logging.INFO)

orientation_choices = ['upright', 'inverted', 'none']
curvature_types = ['mean', 'gaussian', 'min', 'max']

curvature_properties = ['mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max']
curvature_dict = dict(zip(curvature_types, curvature_properties))

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('segmentation_filename', help='path to the segmented image on which to compute volumes')
parser.add_argument('-o', '--microscope-orientation', help='whether the image comes from an upright or inverted microscope', type=str, default='none', choices=orientation_choices)
parser.add_argument('-v', '--minimum-volume', help='lower bound of cell volume heatmap (in µm3)', type=float, default=80.)
parser.add_argument('-V', '--maximum-volume', help='upper bound of cell volume heatmap (in µm3)', type=float, default=400.)
parser.add_argument('-c', '--volume-colormap', help='matplotlib colormap to apply to cell volume heatmap', type=str, default='plasma')
parser.add_argument('-k', '--curvature-type', help='which curvature measure to plot', type=str, default='gaussian', choices=curvature_types)
parser.add_argument('-n', '--curvature-neighborhood', help='size of the vertex ring considered to compute curvature', type=int, default=3)
parser.add_argument('-l', '--surface-edge-length', help='target length for the triangle edges of the surface mesh', type=float, default=5.)
parser.add_argument('-f', '--figure-size', help='size of the figure to save', type=float, default=10.)

args = parser.parse_args()

#############################
# filename definitions :
#############################

segmentation_filename = args.segmentation_filename

orientation_dict = {'upright':1, 'inverted':-1, 'none':None}
orientation = orientation_dict[args.microscope_orientation]

filename = os.path.splitext(segmentation_filename.split('/')[-1])[0]
if segmentation_filename.endswith('.inr.gz'):
    filename = os.path.splitext(filename)[0]
if filename.endswith('_wat'):
    filename = filename[:-4]
if '/' in segmentation_filename:
    dirname = os.path.join(*segmentation_filename.split('/')[:-1])
else:
    dirname = '.'
if segmentation_filename[0] == '/':
    dirname = '/' + dirname

volume_range = (args.minimum_volume, args.maximum_volume)

volume_colormap = args.volume_colormap
try:
    get_cmap(volume_colormap)
except ValueError:
    volume_colormap = 'plasma'

#############################
# main :
#############################

seg_img = imread(segmentation_filename)

assert is_segmented(seg_img.get_array())
seg_img = LabelledImage(seg_img, no_label_id=0)

if orientation is None:
    orientation = 1 - 2*(np.mean(seg_img[:,:,0]) > np.mean(seg_img[:,:,-1]))

cell_center = compute_centers(seg_img, real=True)
cell_volume = compute_volumes(seg_img, real=True)

cell_neighbors = compute_surfel_neighbors(seg_img)
cell_layer = compute_layers(seg_img, cell_neighbors=cell_neighbors)

surface_topomesh = _segmentation_surface_mesh(
    seg_img, microscope_orientation=-orientation, maximal_length=args.surface_edge_length,
    compute_curvature=True, curvature_neighborhood=args.curvature_neighborhood
)

surface_filename = dirname + '/' + filename + "_surface_mesh.ply"
properties_to_save={
    0:['normal']+curvature_properties,
    1:['length'],
    2:['area', 'normal']+curvature_properties,
    3:[]
}
save_ply_property_topomesh(surface_topomesh, surface_filename, properties_to_save=properties_to_save)

cell_curvature = compute_surface_curvature(seg_img, surface_topomesh=surface_topomesh, orientation=orientation, cell_neighbors=cell_neighbors)

cell_df = pd.DataFrame({'label':list(cell_layer.keys())})
for k,dim in enumerate('xyz'):
    cell_df['center_'+dim] = [cell_center[c][k] for c in cell_df['label'].values]
cell_df['volume'] = [cell_volume[c] for c in cell_df['label'].values]
cell_df['layer'] = [cell_layer[c] for c in cell_df['label'].values]
for curvature_name in cell_curvature.keys():
    cell_df[curvature_name] = [cell_curvature[curvature_name][c] for c in cell_df['label'].values]
    
data_filename = dirname + '/' + filename + "_cell_data.csv"
cell_df.to_csv(data_filename,index=False)

labels = [l for l in seg_img.labels() if l!=1]
x_labels = [c for c in labels if orientation*cell_center[c][0] > orientation*(0.5-0.02*orientation)*seg_img.extent[0]]
y_labels = [c for c in labels if orientation*cell_center[c][1] < orientation*(0.5+0.02*orientation)*seg_img.extent[1]]

l1_labels = [c for c in labels if cell_layer[c] == 1]
l2_labels = [c for c in labels if cell_layer[c] == 2]
l3_labels = [c for c in labels if cell_layer[c] == 3]
l4_labels = [c for c in labels if cell_layer[c] == 4]

if orientation is None:
    orientation = 1 - 2 * (np.mean(seg_img[:, :, 0]) > np.mean(seg_img[:, :, -1]))

cell_polydatas = image_to_vtk_cell_polydatas(seg_img,
                                             labels=labels,
                                             subsampling=[int(np.round(0.6/v)) for v in seg_img.voxelsize],
                                             smoothing=10)

figure = plt.figure(0)
figure.clf()

figure.add_subplot(4, 5, 1)

actors = []
cell_polydata = vtk_combine_polydatas([cell_polydatas[l] for l in labels])
cell_actor = vtk_actor(cell_polydata, colormap='glasbey', value_range=(0,255))
actors += [cell_actor]

top_view = vtk_image_actors(actors, focal_point=(0,0,orientation), view_up=(0,-orientation,0))
figure.gca().imshow(top_view[::-1])
figure.gca().axis('off')
figure.gca().set_title("Image Segmentation", size=20)

figure.add_subplot(4, 5, 2)

plot_image_surface_segmentation(figure, orientation=orientation, seg_img=seg_img,
                                plot_type='image', plot_labels=True, alpha=1,
                                colormap='glasbey', value_range=(0,255))

figure.gca().set_xlim(0,seg_img.extent[0])
figure.gca().set_ylim(0,seg_img.extent[1])
figure.gca().axis('off')
figure.gca().set_title("Image Segmentation Surface", size=20)

figure.add_subplot(4, 5, 3)

actors = []
x_polydata = vtk_combine_polydatas([cell_polydatas[l] for l in x_labels])
x_actor = vtk_actor(x_polydata, colormap='glasbey', value_range=(0,255))
actors += [x_actor]

x_view = vtk_image_actors(actors, focal_point=(orientation,0,0),view_up=(0,0,-orientation))
figure.gca().imshow(x_view[::-1])
figure.gca().axis('off')
figure.gca().set_title("YZ Image Segmentation", size=20)

figure.add_subplot(4, 5, 4)

actors = []
y_polydata = vtk_combine_polydatas([cell_polydatas[l] for l in y_labels])
y_actor = vtk_actor(y_polydata, colormap='glasbey', value_range=(0,255))
actors += [y_actor]

y_view = vtk_image_actors(actors, focal_point=(0,-orientation,0), view_up=(0,0,-orientation))
figure.gca().imshow(y_view[::-1])
figure.gca().axis('off')
figure.gca().set_title("XZ Image Segmentation", size=20)

layer_polydatas = image_to_vtk_cell_polydatas(seg_img,
                                              labels=labels,
                                              cell_property=cell_layer,
                                              cell_polydatas=cell_polydatas)

figure.add_subplot(4, 5, 5)

actors = []

cell_actor = vtk_actor(cell_polydata, colormap='glasbey', value_range=(0,255), opacity=0)
actors += [cell_actor]

face_actor = VtkActorTopomesh(surface_topomesh, 2)
face_actor.update(plain_colormap('lightgrey'))
actors += [face_actor]

edge_actor = VtkActorTopomesh(surface_topomesh, 1, line_glyph='line')
edge_actor.update(colormap=plain_colormap('k'), opacity=1)
actors += [edge_actor]

normal_actor = VtkActorTopomesh(surface_topomesh, 0, property_name='normal', vector_glyph='arrow', glyph_scale=args.surface_edge_length/2)
normal_actor.update(colormap=plain_colormap('g'))
actors += [normal_actor]

surface_mesh_view = vtk_image_actors(actors, focal_point=(0,0,orientation), view_up=(0,-orientation,0))
figure.gca().imshow(surface_mesh_view[::-1])
figure.gca().axis('off')
figure.gca().set_title("Image Surface Mesh", size=20)

figure.add_subplot(4, 5, 6)

actors = []
x_polydata = vtk_combine_polydatas([layer_polydatas[l] for l in x_labels])
x_actor = vtk_actor(x_polydata, colormap='jet', value_range=(0,4))
actors += [x_actor]

x_view = vtk_image_actors(actors, focal_point=(orientation,0,0),view_up=(0,0,-orientation))
figure.gca().imshow(x_view[::-1])
figure.gca().axis('off')

figure.add_subplot(4, 5, 7)

actors = []
y_polydata = vtk_combine_polydatas([layer_polydatas[l] for l in y_labels])
y_actor = vtk_actor(y_polydata, colormap='jet', value_range=(0,4))
actors += [y_actor]

y_view = vtk_image_actors(actors, focal_point=(0,-orientation,0), view_up=(0,0,-orientation))
figure.gca().imshow(y_view[::-1])
figure.gca().axis('off')

volume_polydatas = image_to_vtk_cell_polydatas(seg_img,
                                               labels=labels,
                                               cell_property=cell_volume,
                                               cell_polydatas=cell_polydatas)

figure.add_subplot(4, 5, 8)

actors = []
x_polydata = vtk_combine_polydatas([volume_polydatas[l] for l in x_labels])
x_actor = vtk_actor(x_polydata, colormap=volume_colormap, value_range=volume_range)
actors += [x_actor]

x_view = vtk_image_actors(actors, focal_point=(orientation,0,0),view_up=(0,0,-orientation))
figure.gca().imshow(x_view[::-1])
figure.gca().axis('off')

figure.add_subplot(4, 5, 9)

actors = []
y_polydata = vtk_combine_polydatas([volume_polydatas[l] for l in y_labels])
y_actor = vtk_actor(y_polydata, colormap=volume_colormap, value_range=volume_range)
actors += [y_actor]

y_view = vtk_image_actors(actors, focal_point=(0,-orientation,0), view_up=(0,0,-orientation))
figure.gca().imshow(y_view[::-1])
figure.gca().axis('off')

figure.add_subplot(4, 5, 10)

actors = []
curvature_property = curvature_dict[args.curvature_type]
curvature_range = (-0.005, 0.005) if curvature_property == 'gaussian_curvature' else (-0.05, 0.05)

cell_actor = vtk_actor(cell_polydata, colormap='glasbey', value_range=(0,255), opacity=0)
actors += [cell_actor]

face_actor = VtkActorTopomesh(surface_topomesh, 2, property_name=curvature_property, property_degree=0)
face_actor.update(colormap='RdBu_r', value_range=curvature_range)
actors += [face_actor]

edge_actor = VtkActorTopomesh(surface_topomesh, 1, line_glyph='line')
edge_actor.update(colormap=plain_colormap('k'), opacity=0.5)
actors += [edge_actor]

surface_mesh_view = vtk_image_actors(actors, focal_point=(0,0,orientation), view_up=(0,-orientation,0))
figure.gca().imshow(surface_mesh_view[::-1])
figure.gca().axis('off')

for layer,layer_labels in zip([1,2,3,4],[l1_labels,l2_labels,l3_labels,l4_labels]):

    figure.add_subplot(4, 5, 10+layer)

    actors = []
    cell_polydata = vtk_combine_polydatas([volume_polydatas[l] for l in labels])
    cell_actor = vtk_actor(cell_polydata, opacity=0)
    actors += [cell_actor]

    cell_polydata = vtk_combine_polydatas([volume_polydatas[l] for l in layer_labels])
    cell_actor = vtk_actor(cell_polydata, colormap=volume_colormap, value_range=volume_range)
    actors += [cell_actor]

    top_view = vtk_image_actors(actors, focal_point=(0,0,orientation), view_up=(0,-orientation,0))
    figure.gca().imshow(top_view[::-1])
    figure.gca().axis('off')

figure.add_subplot(4, 5, 15)

curvature_property = curvature_dict[args.curvature_type]
curvature_range = (-0.005, 0.005) if curvature_property == 'gaussian_curvature' else (-0.05, 0.05)
curvature_polydatas = image_to_vtk_cell_polydatas(seg_img,
                                                  labels=labels,
                                                  cell_property=cell_curvature[curvature_property],
                                                  cell_polydatas=cell_polydatas)

curvature_labels = [l for l in labels if not np.isnan(cell_curvature[curvature_property][l])]

actors = []
cell_polydata = vtk_combine_polydatas([curvature_polydatas[l] for l in labels])
cell_actor = vtk_actor(cell_polydata, opacity=0)
actors += [cell_actor]

cell_polydata = vtk_combine_polydatas([curvature_polydatas[l] for l in curvature_labels])
cell_actor = vtk_actor(cell_polydata, colormap='RdBu_r', value_range=curvature_range)
actors += [cell_actor]

top_view = vtk_image_actors(actors, focal_point=(0,0,orientation), view_up=(0,-orientation,0))
figure.gca().imshow(top_view[::-1])
figure.gca().axis('off')


cell_layer[1] = 0
all_layer_img = LabelledImage(SpatialImage(array_dict(cell_layer).values(seg_img.get_array()), voxelsize=seg_img.voxelsize), no_label_id=0)

for layer,layer_labels in zip([1,2,3,4],[l1_labels,l2_labels,l3_labels,l4_labels]):

    layer_seg_img = seg_img.copy()
    layer_seg_img[all_layer_img < layer] = 1

    figure.add_subplot(4, 5, 15+layer)

    plot_image_surface_segmentation(figure, orientation=orientation, seg_img=layer_seg_img,
                                    plot_type='image', plot_labels=True, alpha=1,
                                    cell_property=cell_volume, colormap=volume_colormap, value_range=volume_range)

    figure.gca().set_xlim(0,seg_img.extent[0])
    figure.gca().set_ylim(0,seg_img.extent[1])
    figure.gca().axis('off')

cax = figure.gca().inset_axes([0.95, 0.05, 0.03, 0.25])
cbar = ColorbarBase(cax, orientation='vertical', cmap=get_cmap(volume_colormap),
                   norm=Normalize(vmin=volume_range[0], vmax=volume_range[1]))
cax.yaxis.set_ticks_position('left')
cbar.set_label('Volume ($\mu m^3$)',size=16)

figure.add_subplot(4, 5, 20)

plot_image_surface_segmentation(figure, orientation=orientation, seg_img=seg_img,
                                plot_type='image', plot_labels=True, alpha=1,
                                cell_property=cell_curvature[curvature_property],
                                colormap='RdBu_r', value_range=curvature_range)

figure.gca().set_xlim(0, seg_img.extent[0])
figure.gca().set_ylim(0, seg_img.extent[1])
figure.gca().axis('off')

cax = figure.gca().inset_axes([0.95, 0.05, 0.03, 0.25])
cbar = ColorbarBase(cax, orientation='vertical', cmap=get_cmap('RdBu_r'),
                   norm=Normalize(vmin=curvature_range[0], vmax=curvature_range[1]))
cax.yaxis.set_ticks_position('left')
curvature_unit = "$\mu m^{-2}$" if curvature_property == 'gaussian_curvature' else "$\mu m^{-1}$"
cbar.set_label("{} ({})".format(curvature_property.replace("_", " ").title(), curvature_unit), size=16)

figure.set_size_inches(5*args.figure_size, 4*args.figure_size)
figure.tight_layout()
figure_filename = dirname + '/' + filename + "_segmentation_3d_layer_volumes.png"
figure.savefig(figure_filename)