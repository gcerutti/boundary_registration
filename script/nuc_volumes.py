#!/usr/bin/env python

import os
import argparse
import logging
from time import time as current_time

import numpy as np
import pandas as pd

import matplotlib.pyplot as plt
from matplotlib.colorbar import ColorbarBase
from matplotlib.colors import Normalize
from matplotlib.cm import get_cmap
from mpl_toolkits.axes_grid1.inset_locator import inset_axes


from titk_tools.io import imread, imsave
from titk_tools.segmentation import is_segmented, SpatialImage
from titk_tools.quantification import LabelledImage, compute_centers, compute_volumes, compute_surfaces, compute_diameters, compute_surfel_neighbors, compute_layers
from titk_tools.geometry import compute_surface_curvature

from cellcomplex.utils import array_dict

logging.getLogger().setLevel(logging.INFO)

orientation_choices = ['upright', 'inverted', 'none']

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('cell_segmentation_filename', help='path to the segmented image on which to compute volumes')
parser.add_argument('-nuc', '--nuclei-segmentation-filename', help='path to the nuclei segmentation image', type=str, default='None')
parser.add_argument('-o', '--microscope-orientation', help='whether the image comes from an upright or inverted microscope', type=str, default='none', choices=orientation_choices)
#parser.add_argument('-v', '--minimum-volume', help='lower bound of cell volume heatmap (in µm3)', type=float, default=80.)
#parser.add_argument('-V', '--maximum-volume', help='upper bound of cell volume heatmap (in µm3)', type=float, default=400.)


args = parser.parse_args()

#############################
# filename definitions :
#############################

segmentation_filename = args.cell_segmentation_filename
nuc_filename = args.nuclei_segmentation_filename

orientation_dict = {'upright':1, 'inverted':-1, 'none':None}
orientation = orientation_dict[args.microscope_orientation]

filename = os.path.splitext(segmentation_filename.split('/')[-1])[0]
if segmentation_filename.endswith('.inr.gz'):
    filename = os.path.splitext(filename)[0]
if filename.endswith('_wat'):
    filename = filename[:-4]
if '/' in segmentation_filename:
    dirname = os.path.join(*segmentation_filename.split('/')[:-1])
else:
    dirname = '.'
if segmentation_filename[0] == '/':
    dirname = '/' + dirname

#volume_range = (args.minimum_volume, args.maximum_volume)

#############################
# main :
#############################

# computing cell properties
print('----- computing cell properties ------')
seg_img = imread(segmentation_filename)

assert is_segmented(seg_img.get_array())
seg_img = LabelledImage(seg_img, no_label_id=0)

if orientation is None:
    orientation = 1 - 2*(np.mean(seg_img[:,:,0]) > np.mean(seg_img[:,:,-1]))

cell_center = compute_centers(seg_img, real=True)
cell_volume = compute_volumes(seg_img, real=True)
cell_surface = compute_surfaces(seg_img, real=True)
cell_Dmax = compute_diameters(seg_img, real=True)

cell_neighbors = compute_surfel_neighbors(seg_img)
cell_layer = compute_layers(seg_img, cell_neighbors=cell_neighbors)
cell_curvature = compute_surface_curvature(seg_img, orientation=orientation, cell_neighbors=cell_neighbors)

# organise data in a dataframe
cell_df = pd.DataFrame({'label':list(cell_layer.keys())})
for k,dim in enumerate('xyz'):
    cell_df['center_'+dim] = [cell_center[c][k] for c in cell_df['label'].values]
cell_df['volume'] = [cell_volume[c] for c in cell_df['label'].values]
cell_df['surface'] = [cell_surface[c] for c in cell_df['label'].values]
cell_df['Dmax'] = [cell_Dmax[c] for c in cell_df['label'].values]
cell_df['layer'] = [cell_layer[c] for c in cell_df['label'].values]
for curvature_name in cell_curvature.keys():
    cell_df[curvature_name] = [cell_curvature[curvature_name][c] for c in cell_df['label'].values]


# computing nuclei properties
print('----- computing nuclei properties ------')
nuc_img = imread(nuc_filename)

assert is_segmented(nuc_img.get_array())
nuc_img = LabelledImage(nuc_img, no_label_id=0)

nuc_center = compute_centers(nuc_img, real=True)
nuc_volume = compute_volumes(nuc_img, real=True)
nuc_surface = compute_surfaces(nuc_img, real=True)
nuc_Dmax = compute_diameters(nuc_img, real=True)


nuc_df = pd.DataFrame({'label':list(nuc_volume.keys())})

for k,dim in enumerate('xyz'):
    nuc_df['nuc_center_'+dim] = [nuc_center[c][k] for c in nuc_df['label'].values]
nuc_df['nuc_volume'] = [nuc_volume[c] for c in nuc_df['label'].values]
nuc_df['nuc_surface'] = [nuc_surface[c] for c in nuc_df['label'].values]
nuc_df['Dmax'] = [nuc_Dmax[c] for c in nuc_df['label'].values]

# writing results into textfile  
result=pd.merge(cell_df,nuc_df,on='label')
#cell_df.to_csv("cells.csv",index=False)
#nuc_df.to_csv("nuc.csv",index=False)
data_filename = dirname + '/' + filename + "_nuc_data.csv"
result.to_csv(data_filename,index=False)


