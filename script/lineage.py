#!/usr/bin/env python

import os
import argparse
import time
import logging
import pandas as pd
import matplotlib.pyplot as plt

from titk_tools.io import imread
from titk_tools.registration import compute_lineage

logging.getLogger().setLevel(logging.INFO)

parser = argparse.ArgumentParser()
#parser.add_argument('data_dirname', help='path to the image data to process')
parser.add_argument('data_fname', help='path to a textfile containing the list of image filenames')
parser.add_argument('-watdir', '--wat-dirname', help='path to the directory of segmentations', type=str, default='.')
args = parser.parse_args()

start = time.time()

watdir = args.wat_dirname
fname = args.data_fname

###################################################
# Input data:
###################################################

# filenames
sequence_name = "seq"

# times to register
df=pd.read_csv(fname, delimiter=',')

file_times = list(df['T'])
wat_names = list(df['watname'])
watdeformed_names = list(df['wat_on_i+1'])

filenames = [sequence_name + "_T" + str(t).zfill(2) + 'h' for t in file_times]


###################################################
# Pairwise lineages:
###################################################

## d_lineage_list is a list of dictionaries, which stores all the pairwise lineages
d_lineage_list = []
for i in range(len(file_times)-1):
	print("================================")
	print("Lineage extraction: T"+ str(file_times[i]).zfill(2)+" ---> T="+str(file_times[i+1]).zfill(2))
	print("================================")
	img_at_i_file = watdeformed_names[i]
	img_at_next_i_file = watdir+'/'+wat_names[i+1]
	outputfile = "lineage_T" + str(file_times[i]).zfill(2)+"_on_T"+str(file_times[i+1]).zfill(2)+".csv"
	
	img_at_i = imread(img_at_i_file)
	img_at_next_i = imread(img_at_next_i_file)
	
	pairwise_lineage = compute_lineage(img_at_i, img_at_next_i, outputfile)
	d_lineage_list.append(pairwise_lineage)
d_lineage_list.reverse()


###################################################
# Complete lineage:
###################################################

## Merging the pairwise lineage dictionaries to form a complete lineage in d_final
d_final = {}
for key,values in d_lineage_list[0].items():
	list_ancest = []
	list_ancest.append(values)
	d_final[key] = list_ancest	
	found = 0
	k = -1
	for d in d_lineage_list[1:]:
		k+=1
		for key1,values1 in d.items():
			if(key1 == d_final[key][k]):
				list_ancest.append(values1)
				d_final[key] = list_ancest
				found += 1
		if(found == 0):
			list_ancest.append('None')
			d_final[key] = list_ancest		
		found = 0	

## Storing results of d_final in a txt file
complete_lineage = open('complete_lineage.csv','w')
file_times.reverse()

for t in file_times:
	complete_lineage.write("T"+str(t)+",")
complete_lineage.write("\n")

for key,value in d_final.items():
	complete_lineage.write(str(key)+",")
	for i in value:
		complete_lineage.write(str(i)+",")
	complete_lineage.write("\n")
complete_lineage.close()
