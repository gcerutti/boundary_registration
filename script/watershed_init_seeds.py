#!/usr/bin/env python

import os
import argparse
import logging

import numpy as np
import matplotlib.pyplot as plt

from titk_tools.io import imread, imsave, LabelledImage, SpatialImage
from titk_tools.segmentation import filtering, segmentation_watershed_from_seeds
from titk_tools.visualization import plot_image, plot_image_surface_segmentation
from titk_tools.visualization import plot_image_slice, plot_segmentation_slice

from titk_tools.quantification import compute_centers

logging.getLogger().setLevel(logging.INFO)

filter_choices = ['asf', 'gaussian', 'none']
orientation_choices = ['upright', 'inverted', 'none']

parser = argparse.ArgumentParser(formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument('image_filename', help='path to the image to segment')
parser.add_argument('-s', '--save-files', help='whether to save intermediary files', default=False, action='store_true')
parser.add_argument('-p', '--plot-images', help='whether to plot segmented images', default=True, action='store_false')
parser.add_argument('-o', '--microscope-orientation', help='whether the image comes from an upright or inverted microscope', type=str, default='none', choices=orientation_choices)
parser.add_argument('-wf', '--watershed-filter', help='filter to apply to the image before watershed', type=str, default="gaussian", choices=filter_choices)
parser.add_argument('-ws', '--watershed-sigma', help='characteristic size for the watershed pre-filter (in µm)', type=float, default=0.25)
parser.add_argument('-v', '--minimum_volume', help='volume under which small cells will be removed (in µm3)', type=float, default=10)
parser.add_argument('-V', '--maximum_volume', help='volume over which big cells will be merged to the background (in µm3)', type=float, default=None)
parser.add_argument('-seed', '--seed_image', help='image of seeds already labeled', type=str, default=None)

args = parser.parse_args()

#############################
# filename definitions :
#############################
image_filename = args.image_filename
save_files = args.save_files
plot_images = args.plot_images

orientation_dict = {'upright':1, 'inverted':-1, 'none':None}
orientation = orientation_dict[args.microscope_orientation]

filename = image_filename.split('/')[-1].split('.')[0]
if '/' in image_filename:
    dirname = os.path.join(*image_filename.split('/')[:-1])
else:
    dirname = '.'
output_dirname = dirname + "/" + filename + "_wat_from_seeds"
if image_filename[0] == '/':
    output_dirname = '/' + output_dirname

# create a directory and put all the result files in it
if not os.path.exists(output_dirname):
    os.makedirs(output_dirname)
print("results are written in the directory ", output_dirname)

#############################
# parameters :
#############################

seed_filename = args.seed_image

filter_type_wat = args.watershed_filter
filter_value_wat = args.watershed_sigma

min_volume = args.minimum_volume
max_volume = args.maximum_volume


#############################
# main :
#############################

img = imread(image_filename)

if plot_images:
    figure = plt.figure(0)
    figure.clf()

    figure.add_subplot(1, 3, 1)
    plot_image(figure, img, projection='surface', colormap='Greys', value_range=(0,255), orientation=orientation)
    figure.gca().set_title("Image Surface", size=20)

# Filtering
# -----------------------------


if filter_type_wat != 'none':
    wat_string = '_wat-'+filter_type_wat+str(filter_value_wat)
    img_wat = filtering(img, filter_type_wat, filter_value_wat, real=True)

    if save_files:
        # saving the image filtered for watershed
        wat_image_filename = output_dirname + '/' + filename + wat_string + '.inr.gz'
        imsave(wat_image_filename, img_wat)
else:
    wat_string = ''
    img_wat = img

# watershed
# -----------------------------
param_string = ""
param_string += wat_string

seeds = imread(seed_filename)
seeds=LabelledImage(seeds, no_label_id=0)



seed_centers = compute_centers(seeds, real=False)
seed_centers[1] = (0,0,0)
labellist=list(np.unique(seeds))

seedpoints=np.zeros_like(seeds)

for label in labellist :
	cm=seed_centers[label]
	seedpoints[int(cm[0]), int(cm[1]), int(cm[2])]=label
seeds=LabelledImage(seedpoints, no_label_id=0)


seg_img, seed_img = segmentation_watershed_from_seeds(seeds, img_wat, min_volume, max_volume, real=True)
print("There are ", len(np.unique(seg_img)) - 1, " cells detected.")

# saving the watershed
segmentation_filename = output_dirname + '/' + filename + param_string + '_wat.inr.gz'
# put the label 1 to 0
seg_img[seg_img==1]=0
print("Saving the segmented image at ",segmentation_filename)
imsave(segmentation_filename, seg_img)

if save_files:
	# saving the seeds
	seeds_filename = output_dirname + '/' + filename + param_string + '_newseeds.inr.gz'
	imsave(seeds_filename, seed_img)

if plot_images:
	figure.add_subplot(1, 3, 2)
	plot_image(figure, img, projection='surface', orientation=orientation, colormap='Greys', value_range=(0,255))
	plot_image_surface_segmentation(figure, seg_img=seg_img, img=img, orientation=orientation)
	figure.gca().set_title("Image Surface Segmentation", size=20)

	figure.add_subplot(1, 3, 3)
	plot_image_surface_segmentation(figure, orientation=orientation, seg_img=seg_img, img=None)
	figure.gca().set_title("Image Segmentation Surface", size=20)

	figure.set_size_inches(30, 10)
	figure.tight_layout(rect=[0, 0, 1, 0.98])
	figure_filename = output_dirname + '/' + filename + param_string + "_segmentation.png"
	figure.savefig(figure_filename)

if plot_images:
	figure = plt.figure(2)
	figure.clf()

	figure.add_subplot(2, 2, 1)
	plot_image_slice(figure, img, axis='x', orientation=orientation, colormap='Greys', value_range=(0,255))
	figure.gca().set_title("YZ Image Slice", size=20)

	figure.add_subplot(2, 2, 2)
	plot_image_slice(figure, img, axis='x', orientation=orientation, colormap='Greys', value_range=(0,255))
	plot_segmentation_slice(figure, seg_img, axis='x', orientation=orientation, colormap='glasbey')
	figure.gca().set_title("YZ Image Slice Segmentation", size=20)

	figure.add_subplot(2, 2, 3)
	plot_image_slice(figure, img, axis='y', orientation=orientation, colormap='Greys', value_range=(0,255))
	figure.gca().set_title("XZ Image Slice", size=20)

	figure.add_subplot(2, 2, 4)
	plot_image_slice(figure, img, axis='y', orientation=orientation, colormap='Greys', value_range=(0,255))
	plot_segmentation_slice(figure, seg_img, axis='y', orientation=orientation, colormap='glasbey')
	figure.gca().set_title("XZ Image Slice Segmentation", size=20)

	figure.set_size_inches(20, 10)
	figure.tight_layout(rect=[0, 0, 1, 0.98])
	figure_filename = output_dirname + '/' + filename + param_string + "_slice_segmentation.png"
	figure.savefig(figure_filename)
