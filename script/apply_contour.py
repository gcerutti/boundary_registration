#!/usr/bin/env python

import os
import argparse
import logging

import numpy as np
import scipy.ndimage as nd

from titk_tools.io import imread, imsave, SpatialImage
from titk_tools.segmentation import fill_holes_bottom, most_frequent_below_tissuesurface

logging.getLogger().setLevel(logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('image_filename', help='path to the image to segment')
parser.add_argument('-lsm', '--levelset', help='path to level set contour image')

args = parser.parse_args()


# Filename definitions
# ----------------------------
image_filename = args.image_filename
lsm_filename = args.levelset

print("image filename=",image_filename)
print("contour filename=",lsm_filename)

filename = image_filename.split('/')[-1].split('.')[0]
output_filename = filename + "_modified.inr.gz"

image = imread(image_filename)
outspace = imread(lsm_filename)

newimage = np.copy(image)
# Preparing the modified image
# ----------------------------
outspace = fill_holes_bottom(outspace, zlevel=None, outside=True, criterion='corner', orientation=1)

contour =(nd.laplace(outspace)>0).astype('uint8')

most_frequent = most_frequent_below_tissuesurface(image, outspace)
print("most_frequent=",most_frequent)
# emptying outspace
#print("image values :",np.unique(newimage))
#print("outspace values :",np.unique(outspace))
newimage[outspace==1]=most_frequent
# creating the tissue contour
dtyp=image.dtype
print(dtyp)
if dtyp=='uint8':
	newimage[contour==1]=255
else :
	newimage[contour==1]=255*255
# saving the modified image
#print("newimage values :",np.unique(newimage))
#newimage=contour
newimage=SpatialImage(newimage, dtype=dtyp, voxelsize=image.voxelsize, origin=[0, 0, 0])
imsave(output_filename,newimage)

