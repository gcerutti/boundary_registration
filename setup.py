#!/usr/bin/env python
# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

short_descr = "A library of functions useful for MARS, based on the timagetk python module."
readme = open('README.md').read()

# find packages
pkgs = find_packages('src')

setup_kwds = dict(
    name='titk_tools',
    version="0.0.1",
    description=short_descr,
    long_description=readme,
    author="Annamaria Kiss",
    author_email="annamaria.kiss@ens-lyon.fr",
    url='https://gitlab.com/gcerutti/boundary_registration',
    license='cecill-c',
    zip_safe=False,

    packages=pkgs,
    
    package_dir={'': 'src'},
    setup_requires=[],
    install_requires=[],
    tests_require=[],
    entry_points={},
    keywords='',
    
    test_suite='pytest',
    )

setup_kwds['entry_points']['console_scripts'] = []
# setup_kwds['entry_points']['console_scripts'] += ["watershed_v2 = titk_tools.script.watershed_v2:main"]

setup(**setup_kwds)
