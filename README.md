# Boundary analysis in 3D


## Authors:
* Guillaume Cerutti (<guillaume.cerutti@ens-lyon.fr>)
* Annamaria Kiss (<annamaria.kiss@ens-lyon.fr>)


## Installation

### Pre-requisite : Install conda

* Open a terminal window and type `conda`. If no error message appear (but a long how-to message) then you have successfully installed `conda`.

* Otherwise, you may choose to install either [the miniconda tool](https://docs.conda.io/en/latest/miniconda.html) or [the anaconda distribution](https://docs.anaconda.com/anaconda/install/) suitable for your OS.

### Download the source repository

#### Using the `git` command line tool

* Open a terminal window and navigate to the directory of your choice using the `cd` command.

* Copy the source repository using `git` by typing:

```
git clone https://gitlab.inria.fr/gcerutti/boundary_registration.git
```

#### Using the download link on Gitlab

* Click on the **Download** link (left to the **Clone** button) on top of this page to download the sources as an archive.

* Decompress the archive in the directory of your choice.

### Create a new conda environment

* In the terminal window, go to the directory where you copied the source repository:

```
cd boundary_registration
```

* Create a new environment containing all the script dependencies using the provided YAML file:

```
conda env create -f environment.yml
```

### Install the `titk_tools` package

* The first time you use the scripts, you need to install the Python library `titk_tools` supplied with this package to be able to run them:

```
conda activate boundary_registration
python setup.py develop
``` 

### In case

* If you want to use the anisotropic filter, follow the "AniFilters":https://forge.cbp.ens-lyon.fr/redmine/projects/anifilters webpage.

* If you want to use the level set contour, follow the "lsm3d":https://forge.cbp.ens-lyon.fr/redmine/projects/lsm3d webpage.


### Documentation of timagetk

https://mosaic.gitlabpages.inria.fr/timagetk/

## Usage

### 0. Preliminaries

* **Activate the conda environment** Each time you open a new terminal window to use a script, you will need to activate the environment you created to access the dependencies

```
conda activate boundary_registration
```

* **Getting help** Scripts are in the script directory. You can print how to use each script by typing in the terminal the name of the script followed by -h.

### 1. Resample your image isotropically

```
isotropic_resampling.py -isotype avg image_filename
```
will resample your image so that voxels have the same size in all directions, while keeping the voxelvolume constant. The output image will be a .inr.gz image.

### 2. Smooth your image (using AniFilters)

```
ani3D image.inr.gz 0.2 3 1 10 21
```
You may want to adapt the parameters. For the definition of the parameters see the "AniFilters":https://forge.cbp.ens-lyon.fr/redmine/projects/anifilters webpage. This will create a directory with filtered images at different timestep. Choose your favorite as the one that you will use for the next steps. For instance iteration 20 is already smoothed and not too far from the original image. If you do not want to use the level set contour you may skip point 3. and 4. and go to 5. to do the segmentation.

### 3. Create the level set contour of the object (using lsm3d)

```
lsm_contour image.inr.gz 20 10 0 0 1 0.002 -0.002
```
You may want to adapt the parameters. For the definition of the parameters see the "lsm3d":https://forge.cbp.ens-lyon.fr/redmine/projects/lsm3d webpage. This will create a directory and the level set contour will be placed therein.

### 4. Apply the contour on the image

```
apply_contour.py image.inr.gz -lsm contour.inr.gz
```
This will create a new image to segment, where the outer contour is superposed on the image. The result is written in a file called _image_modified.inr.gz_.

### 5. Do a 3D cellular segmentation

Recall the usage of the watershed.py script by typing

```
watershed.py -h
```

It will print in your terminal:

```
usage: watershed.py [-h] [-s] [-p] [-hm HMIN_VALUES [HMIN_VALUES ...]]
                    [-sf {asf,gaussian,none}] [-ss SEED_SIGMA]
                    [-wf {asf,gaussian,none}] [-ws WATERSHED_SIGMA]
                    [-v MINIMUM_VOLUME] [-V MAXIMUM_VOLUME]
                    image_filename

positional arguments:
  image_filename        path to the image to segment

optional arguments:
  -h, --help            show this help message and exit
  -s, --save-files      whether to save intermediary files
  -p, --plot-images     whether to plot segmented images
  -hm HMIN_VALUES [HMIN_VALUES ...], --hmin-values HMIN_VALUES [HMIN_VALUES ...]
                        values of the h-min parameter for the seed extraction
  -sf {asf,gaussian,none}, --seed-filter {asf,gaussian,none}
                        filter to apply to the image before seed extraction
  -ss SEED_SIGMA, --seed-sigma SEED_SIGMA
                        characteristic size for the seed exatraction pre-
                        filter (in µm)
  -wf {asf,gaussian,none}, --watershed-filter {asf,gaussian,none}
                        filter to apply to the image before watershed
  -ws WATERSHED_SIGMA, --watershed-sigma WATERSHED_SIGMA
```

As an example, you might want to use gaussian filter both for the seeds and cellwalls, with higher sigma (0.5) for the seeds than for the walls (0.2), search for seeds with h-min value 2, and save all intermediary files and plots with the following:

```
watershed.py -s -p -hm 2 -sf gaussian -ss 0.5 -wf gaussian -ws 0.2 image_filename
```

### 6. Compute volumes

```
usage: cell_volumes.py [-h] [-o {upright,inverted,none}] [-v MINIMUM_VOLUME]
                       [-V MAXIMUM_VOLUME] [-c VOLUME_COLORMAP]
                       [-k {mean,gaussian,min,max}] [-f FIGURE_SIZE]
                       segmentation_filename

positional arguments:
  segmentation_filename
                        path to the segmented image on which to compute
                        volumes

optional arguments:
  -h, --help            show this help message and exit
  -o {upright,inverted,none}, --microscope-orientation {upright,inverted,none}
                        whether the image comes from an upright or inverted
                        microscope (default: none)
  -v MINIMUM_VOLUME, --minimum-volume MINIMUM_VOLUME
                        lower bound of cell volume heatmap (in µm3) (default:
                        80.0)
  -V MAXIMUM_VOLUME, --maximum-volume MAXIMUM_VOLUME
                        upper bound of cell volume heatmap (in µm3) (default:
                        400.0)
  -c VOLUME_COLORMAP, --volume-colormap VOLUME_COLORMAP
                        matplotlib colormap to apply to cell volume heatmap
                        (default: plasma)
  -k {mean,gaussian,min,max}, --curvature-type {mean,gaussian,min,max}
                        which curvature measure to plot (default: mean)
  -f FIGURE_SIZE, --figure-size FIGURE_SIZE
                        size of the figure to save (default: 10.0)
```



### 7. Interactive plot

```
usage: interactive_plot.py [-h] [-s SEGMENTATION_FILENAME] [-p {ortho,slice}]
                           [-o {upright,inverted,none}]
                           image_filename

positional arguments:
  image_filename        path to the original microscopy image

optional arguments:
  -h, --help            show this help message and exit
  -s SEGMENTATION_FILENAME, --segmentation_filename SEGMENTATION_FILENAME
                        path to the corresponding segmented image
  -p {ortho,slice}, --plot-type {ortho,slice}
                        type of interactive plot to display
  -o {upright,inverted,none}, --microscope-orientation {upright,inverted,none}
                        whether the image comes from an upright or inverted
                        microscope

```

### 8. Sequence registration

Sequence registration registers consecutive timepoints in a timeseries. If you already have segmentations of the different timepoints, it will also apply the registering transformations on the segmentations. This will allow to extract the lineage in the next step.

The names of the image files in the sequence as well as their segmentations should be written in a textfile, having the following structure:
```
T,imgname,watname
0,img0.inr.gz,img0_wat.inr.gz
24,img1.inr.gz,img1_wat.inr.gz
```
You can find a template file, called "filename-index.csv" in the script folder of this repository. If the images and segmentations are all in your working directory, the name of this file is the only argument you need to give to the "register_sequence.py" sequence to work, like this:

```
register_sequence.py filename-index.csv
```

Otherwise let us recall here the usage of the sequence registration script.
```
usage: register_sequence.py [-h] [-s] [-p] [-imgdir IMG_DIRNAME] [-wat]
                            [-watdir WAT_DIRNAME]
                            [-o {o,r,i,e,n,t,a,t,i,o,n,_,c,h,o,i,c,e,s}]
                            [-ph PYRAMID_HIGHEST_LEVEL]
                            [-pl PYRAMID_LOWEST_LEVEL] [-es ELASTIC_SIGMA]
                            [-fs FLUID_SIGMA]
                            data_fname

positional arguments:
  data_fname            path to a textfile containing the list of image
                        filenames

optional arguments:
  -h, --help            show this help message and exit
  -s, --save-files      whether to save intermediary files (default: True)
  -p, --plot-images     whether to plot registered images (default: True)
  -imgdir IMG_DIRNAME, --img-dirname IMG_DIRNAME
                        path to the directory of images to register (default:
                        .)
  -wat, --apply-on-wat  whether to apply transformations on watershed
                        segmentations (default: True)
  -watdir WAT_DIRNAME, --wat-dirname WAT_DIRNAME
                        path to the directory of segmentations (default: .)
  -o {o,r,i,e,n,t,a,t,i,o,n,_,c,h,o,i,c,e,s}, --microscope-orientation {o,r,i,e,n,t,a,t,i,o,n,_,c,h,o,i,c,e,s}
                        whether the image comes from an upright or inverted
                        microscope (default: none)
  -ph PYRAMID_HIGHEST_LEVEL, --pyramid-highest-level PYRAMID_HIGHEST_LEVEL
                        pyramid highest level (3 corresponds to 32x32x32 for
                        an original 256x256x256 image) (default: 6)
  -pl PYRAMID_LOWEST_LEVEL, --pyramid-lowest-level PYRAMID_LOWEST_LEVEL
                        pyramid lowest level (0 corresponds to original image
                        dimension) (default: 1)
  -es ELASTIC_SIGMA, --elastic-sigma ELASTIC_SIGMA
                        sigma for elastic regularization of the transformation
                        (default: 5)
  -fs FLUID_SIGMA, --fluid-sigma FLUID_SIGMA
                        sigma for fluid regularization (field interpolation
                        and regularization for pairings) (default: 3)

```


### 9. Lineage extraction
The sequence registration script will create you a new csv file, named in this case "registered_filename-index.csv", that will contain the paths to the deformed watershed segmentations.

```
lineage.py registered_filename-index.csv
```

will create pairwise lineages as well as a "complete lineage" file. The pairwise lineage file contains in the first column the cell labels at time t, while the following columns contain the labels of the daughter cell(s) at time t+1. The complete lineage file has a column per timepoint, the first column being the last timepoint, and contains all the cell labels at this last timepoint. The following columns contain the label of all ancestors.


