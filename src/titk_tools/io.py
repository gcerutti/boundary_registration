# This is a library of functions useful for MARS
# it is based on the timagetk python module
# https://mosaic.gitlabpages.inria.fr/timagetk
#

import numpy as np

from timagetk.wrapping.bal_trsf import TRSF_TYPE_DICT, TRSF_UNIT_DICT
from timagetk.algorithms.trsf import allocate_c_bal_matrix, create_trsf

from timagetk.components import LabelledImage
# io
from timagetk.io import imread, imsave, SpatialImage
from timagetk.io import read_trsf, save_trsf

# ========================
# IO for rigid and affine transformations
# ========================


def tfsave(fname, tf):
    matrix = tf.mat.to_np_array().astype(float)
    np.savetxt(fname, matrix, delimiter=";")
    return


def matrix2tf(matrix):
	rigid_trsf = create_trsf(param_str_2='-identity', trsf_type=TRSF_TYPE_DICT['RIGID_3D'], trsf_unit=TRSF_UNIT_DICT['REAL_UNIT'])
	allocate_c_bal_matrix(rigid_trsf.mat.c_struct, matrix)
	return rigid_trsf


def tfread(fname):
    matrix = np.loadtxt(fname, delimiter=";")
    rigid_trsf = matrix2tf(matrix)
    return rigid_trsf


'''
def tfread(fname):
    matrix = np.loadtxt(fname, delimiter=";")
    rigid_trsf = create_trsf(param_str_2='-identity', trsf_type=TRSF_TYPE_DICT['RIGID_3D'], trsf_unit=TRSF_UNIT_DICT['REAL_UNIT'])
    allocate_c_bal_matrix(rigid_trsf.mat.c_struct, matrix)
    return rigid_trsf
'''
