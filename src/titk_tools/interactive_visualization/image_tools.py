import numpy as np


def labelled_image_contours(seg_img):
    shape = np.array(seg_img.shape)
    dimension = len(shape)
    neighborhood_shape = np.array([s - 2 for k, s in enumerate(shape)])

    neighborhood_img = []
    if dimension == 3:
        for x in np.arange(-1, 2):
            for y in np.arange(-1, 2):
                for z in np.arange(-1, 2):
                    neighborhood_img.append(np.array(seg_img)[1 + x:shape[0] - 1 + x, 1 + y:shape[1] - 1 + y, 1 + z:shape[2] - 1 + z])
        neighborhood_img = np.sort(np.transpose(neighborhood_img, (1, 2, 3, 0))).reshape((neighborhood_shape).prod(), 27)
    elif dimension == 2:
        for x in np.arange(-1, 2):
            for y in np.arange(-1, 2):
                neighborhood_img.append(np.array(seg_img)[1 + x:shape[0] - 1 + x, 1 + y:shape[1] - 1 + y])
        neighborhood_img = np.sort(np.transpose(neighborhood_img, (1, 2, 0))).reshape((neighborhood_shape).prod(), 9)

    neighborhood_contour = 2 - np.all(neighborhood_img == neighborhood_img[..., :1], axis=-1).astype(int)
    neighborhood_contour = neighborhood_contour.reshape(tuple(list(neighborhood_shape)))

    contour_img = np.zeros_like(np.array(seg_img)).astype(float)
    if dimension == 3:
        contour_img[1:shape[0] - 1, 1:shape[1] - 1, 1:shape[2] - 1] += (neighborhood_contour - 1)
    elif dimension == 2:
        contour_img[1:shape[0] - 1, 1:shape[1] - 1] += (neighborhood_contour - 1)

    return contour_img


def z_slice_image(img, slice_points):
    slice_points = np.array(slice_points)
    slice_vector = slice_points[1] - slice_points[0]
    slice_extent = np.linalg.norm(slice_vector)

    slice_voxelsize = np.mean(img.voxelsize[:2])
    slice_x = np.linspace(slice_points[0, 0], slice_points[1, 0], int(np.ceil(slice_extent / slice_voxelsize)))
    slice_y = np.linspace(slice_points[0, 1], slice_points[1, 1], int(np.ceil(slice_extent / slice_voxelsize)))

    slice_z = np.tile(np.arange(img.shape[2]) * img.voxelsize[2], (len(slice_x), 1))
    slice_x = np.tile(slice_x, (img.shape[2], 1)).T
    slice_y = np.tile(slice_y, (img.shape[2], 1)).T

    slice_coords = np.concatenate(np.transpose([slice_x, slice_y, slice_z], (1, 2, 0)))
    slice_coords = np.round(slice_coords / np.array(img.voxelsize)).astype(int)
    slice_coords = np.maximum(0, np.minimum(np.array(img.shape) - 1, slice_coords))
    slice_coords = tuple(np.transpose(slice_coords))

    slice_img = img.get_array()[slice_coords].reshape(slice_x.shape)

    return slice_img
