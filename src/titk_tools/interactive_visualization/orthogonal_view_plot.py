import numpy as np

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from visu_core.matplotlib import glasbey

from titk_tools.visualization import image_slice
from titk_tools.interactive_visualization.image_tools import labelled_image_contours


class OrthogonalViewPlot(object):

    def __init__(self, figure, img, seg_img=None, x=None, y=None, z=None, colormap='gray', value_range=None, alpha=1.):
        self.figure = figure
        self.img = img
        self.seg_img = seg_img

        self.colormap = colormap
        self.value_range = value_range
        self.alpha = alpha

        if x is None:
            x = img.extent[0] / 2
        if y is None:
            y = img.extent[1] / 2
        if z is None:
            z = img.extent[2] / 2
        self.axis_positions = dict(zip('xyz', [x, y, z]))
        self.axis_plots_axes = dict(zip('xyz', ['zy', 'xz', 'xy']))

        self.axes = {}
        self.update_axes()

        self.drag = False
        self.connects = {}
        self.connect()

        self.update_plot()

    def __del__(self):
        self.clear()

    def update_axes(self):
        self.figure.clf()

        grid = GridSpec(2, 2, figure=self.figure,
                        width_ratios=[self.img.extent[1], self.img.extent[2]],
                        height_ratios=[self.img.extent[0], self.img.extent[2]])
        self.axes['z'] = plt.subplot(grid[0, 0])
        self.axes['x'] = plt.subplot(grid[0, 1])
        self.axes['y'] = plt.subplot(grid[1, 0])

        self.figure.subplots_adjust(wspace=0, hspace=0)
        self.figure.tight_layout()

    def connect(self):
        self.connects['press'] = self.figure.canvas.mpl_connect('button_press_event', self.press)
        self.connects['move'] = self.figure.canvas.mpl_connect('motion_notify_event', self.move)
        self.connects['release'] = self.figure.canvas.mpl_connect('button_release_event', self.release)
        self.connects['scroll'] = self.figure.canvas.mpl_connect('scroll_event', self.scroll)

    def clear(self):
        for c in self.connects.keys():
            self.figure.canvas.mpl_disconnect(self.connects[c])

    def press(self, event):
        if event.inaxes is not None:
            self.drag = True

    def move(self, event):
        if self.drag:
            if event.inaxes is None:
                self.drag = False
            else:
                for axis in 'xyz':
                    if event.inaxes == self.axes[axis]:
                        self.axis_positions[self.axis_plots_axes[axis][0]] = event.xdata
                        self.axis_positions[self.axis_plots_axes[axis][1]] = event.ydata
            self.update_plot()

    def release(self, event):
        self.drag = False

    def scroll(self, event):
        if event.inaxes is not None:
            step = 20 * event.step
            point = [event.xdata, event.ydata]
            xlim = list(event.inaxes.get_xlim())
            point_x = (point[0] - xlim[0]) / (xlim[1] - xlim[0])
            x_size = xlim[1] - xlim[0]
            xlim[0] += step * point_x * x_size / 100.
            xlim[1] -= step * (1 - point_x) * x_size / 100.
            ylim = list(event.inaxes.get_ylim())
            point_y = (point[1] - ylim[0]) / (ylim[1] - ylim[0])
            y_size = ylim[1] - ylim[0]
            ylim[0] += step * point_y * y_size / 100.
            ylim[1] -= step * (1 - point_y) * y_size / 100.
            event.inaxes.set_ylim(*ylim)
            event.inaxes.set_xlim(*xlim)
            self.figure.canvas.draw()

    def update_plot(self):
        axis_colors = dict(zip('xyz', 'rgb'))
        axis_index = dict(zip('xyz', range(3)))

        for axis in 'xyz':
            self.figure.sca(self.axes[axis])
            self.figure.gca().cla()
            slice_img = image_slice(self.img, axis=axis, position=self.axis_positions[axis])
            extent = (0, self.img.extent[axis_index[self.axis_plots_axes[axis][0]]],
                      0, self.img.extent[axis_index[self.axis_plots_axes[axis][1]]])
            if axis in 'x':
                slice_img = slice_img.T[::-1]
                extent = (self.img.extent[axis_index[self.axis_plots_axes[axis][0]]], 0,
                          0, self.img.extent[axis_index[self.axis_plots_axes[axis][1]]])

            if self.value_range is None:
                vmin = self.img.min()
                vmax = self.img.max()
            else:
                vmin, vmax = self.value_range
            self.figure.gca().imshow(slice_img, interpolation='none', alpha=self.alpha,
                                     cmap=self.colormap, vmin=vmin, vmax=vmax,
                                     extent=extent)

            if self.seg_img is not None:
                slice_seg_img = image_slice(self.seg_img, axis=axis, position=self.axis_positions[axis])
                if axis in 'x':
                    slice_seg_img = slice_seg_img.T[::-1]
                slice_seg_img_8bit = (slice_seg_img % 256).astype(float)
                slice_seg_img_8bit[slice_seg_img == 1] = np.nan
                self.figure.gca().imshow(slice_seg_img_8bit, cmap='glasbey', vmin=0, vmax=255,
                                    interpolation='none', extent=extent, alpha=0.2)
                projected_contour_img = labelled_image_contours(slice_seg_img).astype(float)
                projected_contour_img[projected_contour_img == 0] = np.nan
                self.figure.gca().imshow(projected_contour_img, cmap='gray', vmin=0, vmax=0,
                                    interpolation='none', extent=extent, alpha=0.2)

            self.figure.gca().plot(list(extent)[:2],
                                   [self.axis_positions[self.axis_plots_axes[axis][1]] for _ in range(2)],
                                   color=axis_colors[self.axis_plots_axes[axis][1]], linewidth=0.5)
            self.figure.gca().plot([self.axis_positions[self.axis_plots_axes[axis][0]] for _ in range(2)],
                                   list(extent)[2:],
                                   color=axis_colors[self.axis_plots_axes[axis][0]], linewidth=0.5)
            self.figure.gca().plot([extent[0], extent[1], extent[1], extent[0], extent[0]],
                                   [extent[2], extent[2], extent[3], extent[3], extent[2]],
                                   color=axis_colors[axis])
            if axis in 'xz':
                self.figure.gca().xaxis.set_ticks_position('top')
            if axis in 'x':
                self.figure.gca().yaxis.set_ticks_position('right')

        self.figure.canvas.draw()