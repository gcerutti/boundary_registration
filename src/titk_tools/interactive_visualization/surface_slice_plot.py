import numpy as np
import scipy.ndimage as nd

import matplotlib.pyplot as plt
from matplotlib.gridspec import GridSpec

from visu_core.matplotlib import glasbey
from visu_core.matplotlib.colormap import plain_colormap

from titk_tools.visualization import image_surface_projection
from titk_tools.interactive_visualization.image_tools import z_slice_image, labelled_image_contours


class SurfaceSlicePlot(object):

    def __init__(self, figure, img, seg_img=None, orientation=None, xy_left=None, xy_right=None, colormap='gray', value_range=None, alpha=1.):
        self.figure = figure
        self.img = img
        self.seg_img = seg_img

        self.orientation = orientation
        if self.orientation is None:
            self.orientation = 1 - 2 * (np.mean(self.img[:, :, 0]) > np.mean(self.img[:, :, -1]))

        self.projected_img = None
        self.projected_seg_img = None
        self.projected_contour_img = None

        self.colormap = colormap
        self.value_range = value_range
        self.alpha = alpha

        if xy_left is None:
            xy_left = [0, img.extent[1] / 2]
        if xy_right is None:
            xy_right = [img.extent[0], img.extent[1] / 2]
        self.xy_left = xy_left
        self.xy_right = xy_right

        self.left_artist = None
        self.right_artist = None

        self.axes = {}
        self.update_axes()

        self.drag_left = False
        self.drag_right = False
        self.connects = {}
        self.connect()

        self.update_plot()

    def __del__(self):
        self.clear()

    def update_axes(self):
        self.figure.clf()

        grid = GridSpec(2, 1, figure=self.figure,
                        width_ratios=[self.img.extent[0]],
                        height_ratios=[self.img.extent[1], self.img.extent[2]])
        self.axes['surface'] = plt.subplot(grid[0, 0])
        self.axes['slice'] = plt.subplot(grid[1, 0])

        self.figure.subplots_adjust(wspace=0, hspace=0)
        self.figure.tight_layout()

    def connect(self):
        self.connects['press'] = self.figure.canvas.mpl_connect('button_press_event', self.press)
        self.connects['move'] = self.figure.canvas.mpl_connect('motion_notify_event', self.move)
        self.connects['release'] = self.figure.canvas.mpl_connect('button_release_event', self.release)
        self.connects['scroll'] = self.figure.canvas.mpl_connect('scroll_event', self.scroll)

    def clear(self):
        for c in self.connects.keys():
            self.figure.canvas.mpl_disconnect(self.connects[c])

    def press(self, event):
        if event.inaxes == self.axes['surface']:
            if self.left_artist is not None:
                picked, ind = self.left_artist.contains(event)
                if picked:
                    self.drag_left = True
            if self.right_artist is not None:
                picked, ind = self.right_artist.contains(event)
                if picked and not self.drag_left:
                    self.drag_right = True

    def move(self, event):
        if event.inaxes == self.axes['surface']:
            if self.drag_left:
                self.xy_left = (event.xdata, event.ydata)
            elif self.drag_right:
                self.xy_right = (event.xdata, event.ydata)
            if self.drag_left or self.drag_right:
                self.update_plot()
        else:
            self.drag_left = False
            self.drag_right = False

    def release(self, event):
        self.drag_left = False
        self.drag_right = False

    def scroll(self, event):
        if event.inaxes is not None:
            step = 20 * event.step
            point = [event.xdata, event.ydata]
            xlim = list(event.inaxes.get_xlim())
            point_x = (point[0] - xlim[0]) / (xlim[1] - xlim[0])
            x_size = xlim[1] - xlim[0]
            xlim[0] += step * point_x * x_size / 100.
            xlim[1] -= step * (1 - point_x) * x_size / 100.
            ylim = list(event.inaxes.get_ylim())
            point_y = (point[1] - ylim[0]) / (ylim[1] - ylim[0])
            y_size = ylim[1] - ylim[0]
            ylim[0] += step * point_y * y_size / 100.
            ylim[1] -= step * (1 - point_y) * y_size / 100.
            event.inaxes.set_ylim(*ylim)
            event.inaxes.set_xlim(*xlim)
            self.figure.canvas.draw()

    def update_plot(self):
        self.figure.sca(self.axes['surface'])
        self.figure.gca().cla()

        if self.projected_img is None:
            projected_img, coords = image_surface_projection(self.img, orientation=self.orientation, height=5.)
            self.projected_img = np.transpose(projected_img)[::-1]

            if self.seg_img is not None:
                # projected_seg_img, coords = image_surface_projection(self.seg_img, gaussian_sigma=0, threshold=1)
                projected_seg_img = self.seg_img.get_array()[coords].reshape(self.seg_img.shape[:2])
                self.projected_seg_img = np.transpose(projected_seg_img)[::-1]

                self.projected_contour_img = labelled_image_contours(self.projected_seg_img).astype(float)
                self.projected_contour_img[self.projected_contour_img == 0] = np.nan

        if self.value_range is None:
            vmin = self.img.min()
            vmax = self.img.max()
        else:
            vmin, vmax = self.value_range

        self.figure.gca().imshow(self.projected_img, interpolation='none', alpha=self.alpha,
                                 cmap=self.colormap, vmin=vmin, vmax=vmax,
                                 extent=(0, self.img.extent[0], 0, self.img.extent[1]))
        if self.seg_img is not None:
            projected_seg_img_8bit = (self.projected_seg_img % 256).astype(float)
            projected_seg_img_8bit[self.projected_seg_img == 1] = np.nan
            self.figure.gca().imshow(projected_seg_img_8bit, cmap='glasbey', vmin=0, vmax=255,
                                interpolation='none', extent=(0, self.img.extent[0], 0, self.img.extent[1]), alpha=0.2)
            self.figure.gca().imshow(self.projected_contour_img, cmap=plain_colormap('k'), vmin=0, vmax=0,
                                interpolation='none', extent=(0, self.img.extent[0], 0, self.img.extent[1]), alpha=0.2)

        self.figure.gca().plot([self.xy_left[0], self.xy_right[0]],
                               [self.xy_left[1], self.xy_right[1]],
                               color='k', linewidth=0.5)
        self.left_artist = self.figure.gca().scatter(self.xy_left[0], self.xy_left[1],
                                                     color='r', ec='k', s=80, linewidth=0.5, zorder=6)
        self.right_artist = self.figure.gca().scatter(self.xy_right[0], self.xy_right[1],
                                                      color='g', ec='k', s=80, linewidth=0.5, zorder=5)

        self.figure.sca(self.axes['slice'])
        self.figure.gca().cla()

        slice_img = z_slice_image(self.img, [self.xy_left, self.xy_right])
        slice_extent = np.linalg.norm(np.array(self.xy_right) - np.array(self.xy_left))

        self.figure.gca().imshow(slice_img.T[::self.orientation], interpolation='none', alpha=self.alpha,
                                 cmap=self.colormap, vmin=vmin, vmax=vmax,
                                 extent=(0, slice_extent, 0, self.img.extent[2]))

        if self.seg_img is not None:
            slice_seg_img = z_slice_image(self.seg_img, [self.xy_left, self.xy_right]).T[::-1]
            slice_seg_img_8bit = (slice_seg_img % 256).astype(float)
            slice_seg_img_8bit[slice_seg_img == 1] = np.nan
            self.figure.gca().imshow(slice_seg_img_8bit[::-self.orientation], cmap='glasbey', vmin=0, vmax=255,
                                interpolation='none', extent=(0, slice_extent, 0, self.img.extent[2]), alpha=0.2)
            slice_contour_img = labelled_image_contours(slice_seg_img).astype(float)
            slice_contour_img[slice_contour_img == 0] = np.nan
            self.figure.gca().imshow(slice_contour_img[::-self.orientation], cmap=plain_colormap('k'), vmin=0, vmax=0,
                                interpolation='none', extent=(0, slice_extent, 0, self.img.extent[2]), alpha=0.2)

        self.figure.gca().scatter(0, self.img.extent[2], color='r', ec='k', s=80, linewidth=0.5, zorder=6)
        self.figure.gca().scatter(slice_extent, self.img.extent[2], color='g', ec='k', s=80, linewidth=0.5, zorder=5)

        self.figure.canvas.draw()
