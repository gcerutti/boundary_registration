# This is a library of functions useful for MARS
# it is based on the timagetk python module
# https://mosaic.gitlabpages.inria.fr/timagetk
#

import logging

from timagetk.components import SpatialImage, LabelledImage
from timagetk.plugins.resampling import isometric_resampling
from timagetk.algorithms.blockmatching import blockmatching
from timagetk.algorithms.trsf import apply_trsf

import collections
import numpy as np
from scipy import ndimage as nd

# ========================
# Resampling
# ========================


def isotropic_resampling(im, isores):
    im_out = isometric_resampling(im, method=isores, option='grey')
    return im_out


def isotropic_resampling_seg(im, isores):
    im = LabelledImage(im, no_label_id=0)
    im_out = isometric_resampling(im, method=isores, option='label')
    return im_out

# ========================
# Registration
# ========================


def find_rigid_transfo(reference_img, floating_img, init_trsf):
    logging.info("rigid registration...")
    parameters=("-estimator wlts" +
                " -floating-low-threshold 0" +
                " -reference-low-threshold 0" +
                " -pyramid-highest-level 5" +
                " -pyramid-lowest-level 2" +
                " -lts-fraction 0.55")
    rigid_trsf, transformed_img = blockmatching(floating_img, reference_img, init_result_trsf=init_trsf, param_str_2=parameters)
    return transformed_img, rigid_trsf

def find_rigid_transfo2(reference_img, floating_img):
    logging.info("rigid registration...")
    parameters=("-estimator wlts" +
                " -floating-low-threshold 0" +
                " -reference-low-threshold 0" +
                " -pyramid-highest-level 5" +
                " -pyramid-lowest-level 2" +
                " -lts-fraction 0.55")
    rigid_trsf, transformed_img = blockmatching(floating_img, reference_img, param_str_2=parameters)
    return transformed_img, rigid_trsf


def find_affine_transfo(reference_img, floating_img, init_trsf):
    logging.info("affine registration...")
    tftype=" -trsf-type affine"
    parameters=("-estimator wlts" +
                " -floating-low-threshold 0" +
                " -reference-low-threshold 0" +
                " -pyramid-highest-level 3" +
                " -pyramid-lowest-level 2" +
                " -lts-fraction 0.55")
    affine_trsf, transformed_img = blockmatching(floating_img, reference_img, init_result_trsf=init_trsf,
                                                 param_str_1=tftype, param_str_2=parameters)
    return transformed_img, affine_trsf


def find_affine_transfo2(reference_img, floating_img):
    logging.info("affine registration...")
    tftype=" -trsf-type affine"
    parameters=("-estimator wlts" +
                " -floating-low-threshold 0" +
                " -reference-low-threshold 0" +
                " -pyramid-highest-level 3" +
                " -pyramid-lowest-level 2" +
                " -lts-fraction 0.55")
    affine_trsf, transformed_img = blockmatching(floating_img, reference_img,
                                                 param_str_1=tftype, param_str_2=parameters)
    return transformed_img, affine_trsf

def find_nl_transfo(reference_img, floating_img, ph=5, pl=2, es=3.0, fs=3.0):
    logging.info("non-linear registration...")
    bs = 5 # by default = 4
    tftype=" -trsf-type vectorfield"
    parameters=(
            " -estimator wlts" +
            " -py-gf" +
            " -floating-low-threshold 2" +
            " -reference-low-threshold 2" +
            #" -reference-removed-fraction  0.25" +
            #" -block-size " + str(bs) + " " + str(bs) + " " + str(bs) +
            " -pyramid-highest-level "+str(ph) +
            " -pyramid-lowest-level "+str(pl) +
            " -elastic-sigma "+str(es)+
            " -fluid-sigma "+str(fs))
    nl_trsf, transformed_img = blockmatching(floating_img, reference_img,
                                             param_str_1=tftype, param_str_2=parameters)
    return transformed_img, nl_trsf



# ========================
# Transformations
# ========================

def apply_transfo_on_seg1(transfo, img, template=None):
    img_out = apply_trsf(img, transfo, template_img=template, param_str_1="-nearest")
    return img_out

def apply_transfo_on_image(transfo, img, template=None):
    img_out = apply_trsf(img, transfo, template_img=template, param_str_1="-linear")
    return img_out


# ========================
# Lineage
# ========================


def CellVolumes(seeds,ind) :
	weight=np.ones_like(seeds)
	volumes=nd.sum(weight,seeds,ind)
	return volumes
	
def CellProperties(seeds) :
    ind=np.unique(seeds)
    #cells=range(1,index.max()+1)
    weight=np.ones_like(seeds)
    # seeds are considered as labels
    center_mass=nd.center_of_mass(weight,seeds,ind)
    # bounding boxes
    bounding_box=nd.find_objects(seeds)
    removeAll(bounding_box,None)
    return ind, center_mass, bounding_box

def removeAll(the_list,val):
	while val in the_list:
		the_list.remove(val)

def remove_bordercells(wat):
	s = wat.shape
	res = wat.voxelsize
	aux = np.ones_like(wat)
	aux[1:(s[0]-1), 1:(s[1]-1),1:(s[2]-1)] = 0
	border_labels = np.unique(wat*aux)
	wat=np.array(wat)
	for i in border_labels :
		wat[wat==i]=0
	wat = SpatialImage(wat)
	wat.voxelsize = res
	return wat


def compute_lineage(img_at_i, img_at_next_i, outputfile, threshold=0.8) :
	img_at_i = remove_bordercells(img_at_i)
	img_at_next_i = remove_bordercells(img_at_next_i)

	img2_cell_sizes = np.zeros(np.max(img_at_next_i)+1)
	# labels at i
	n = np.unique(img_at_i)[1:]
	# labels at i+1
	m = np.unique(img_at_next_i)

	f = open(outputfile,'w')
	f_log = open(outputfile+'-log.txt','w')

	print ("Compute cell sizes at i+1 stage...")
	img2_cell_sizes_list = CellVolumes(img_at_next_i, m)
	img2_cell_sizes=dict(zip(m,img2_cell_sizes_list))

	print ("Computing the Bounding boxes on deformed image at i....")
	x,y,bounding_box = CellProperties(img_at_i)
	print ("Bounding boxes computed.")
	
	lineage={}
	for k in range(len(n)):
		daughters=[]
		# look for daughter cells of label n[k] in image at i
		x = (img_at_i[bounding_box[k]] == n[k])
		img2_corresponding_cells = x*img_at_next_i[bounding_box[k]]
		img2_corresponding_cells = img2_corresponding_cells.ravel() #convert to 1d
		counter = collections.Counter(img2_corresponding_cells)
		print("For cell %d" % n[k])
		f.write(str(n[k]) + ",")
		for key, value in counter.items():
			if(img2_cell_sizes[key] > 1):
				f_log.write(str(n[k]) + "  " + str(key) + "      " + str((value/img2_cell_sizes[key])) + "\n")
			if (value > threshold*img2_cell_sizes[key] and img2_cell_sizes[key] > 1):
				print ("%s" % key)
				print ("%f" % (value/img2_cell_sizes[key]))
				f.write(str(key) +  ",")
				lineage[key]=n[k]
		f.write("\n")
	f.close()
	f_log.close()
	return lineage


def tf2vf(vector_field_transform):
	vector_field = np.transpose([vector_field_transform.vx.to_spatial_image(), vector_field_transform.vy.to_spatial_image(), vector_field_transform.vz.to_spatial_image()], (1, 2, 3, 0))
	return vector_field


def compute_mean_transformation(tf_list) :
	mean_tf = tf2vf(tf_list[0])
	for i in range(1,len(tf_list)):
		tf = tf2vf(tf_list[i])
		mean_tf += tf
	mean_tf = mean_tf/len(tf_list)
	return mean_tf


'''
vector_field = np.transpose([vector_field_transform.vx.to_spatial_image(),
                                         vector_field_transform.vy.to_spatial_image(),
                                         vector_field_transform.vz.to_spatial_image()], (1, 2, 3, 0))
                              
'''
