import numpy as np
import scipy.ndimage as nd
import matplotlib.pyplot as plt
from matplotlib.cm import ScalarMappable
from matplotlib.colors import Normalize

from skimage.filters import threshold_otsu

from timagetk.components import SpatialImage
from cellcomplex.utils import array_dict

from visu_core.matplotlib import glasbey


def image_surface_projection(img, gaussian_sigma=1., height=3., threshold=None, orientation=None):

    if threshold is None:
        threshold = 0.5*threshold_otsu(img.get_array().mean(axis=2))

    if orientation is None:
        orientation = 1 - 2*(np.mean(img[:,:,0]) > np.mean(img[:,:,-1]))

    smooth_img = nd.gaussian_filter(img.get_array(), gaussian_sigma/np.array(img.voxelsize))

    erosion_iterations = int(height/img.voxelsize[2])
    mask_img = (smooth_img > threshold)
    mask_img = nd.binary_fill_holes(mask_img)
    mask_img = nd.binary_erosion(mask_img, iterations=erosion_iterations, border_value=1)

    if orientation == 1:
        zz = -np.ones_like(img.get_array()[:, :, 0]).astype(np.int16)
        for z in range(img.shape[2]):
            zz[(mask_img[:, :, z] > 0) & (zz < 0)] = z
        zz[zz == -1] = img.shape[2] - 1
    elif orientation == -1:
        zz = img.shape[2] * np.ones_like(img.get_array()[:, :, 0]).astype(np.int16)
        for z in range(img.shape[2])[::-1]:
            zz[(mask_img[:, :, z] > 0) & (zz >= img.shape[2])] = z
        zz[zz == img.shape[2]] = 0

    xx, yy = np.meshgrid(np.arange(img.shape[0]), np.arange(img.shape[1]),indexing='ij')

    zz = nd.gaussian_filter(zz.astype(float), sigma=gaussian_sigma/img.voxelsize[0], mode='nearest')

    coords = np.transpose([xx, yy, zz], (1, 2, 0))
    coords = np.maximum(np.minimum(coords, np.array(img.shape) - 1), 0)
    coords[np.isnan(coords)] = 0
    coords = coords.astype(int)
    coords = tuple(np.transpose(np.concatenate(coords)))

    surface_img = img.get_array()[coords].reshape(xx.shape)

    return surface_img, coords


def image_slice(img, axis='x', position=None):

    axis_index = {dim:k for k,dim in enumerate('xyz')}
    if position is None:
        axis_extents = {dim:(0,img.extent[k]) for k,dim in enumerate('xyz')}
        position =  np.mean(axis_extents[axis])
    voxelsize = img.voxelsize
    position = int(np.round(position/voxelsize[axis_index[axis]]))

    position =  np.maximum(0,np.minimum(img.shape[axis_index[axis]]-1,position))
    if axis=='x':
        slice_img = np.transpose(img.get_array()[position])[::-1]
    elif axis=='y':
        slice_img = np.transpose(img.get_array()[:,position])[::-1]
    elif axis=='z':
        slice_img = np.transpose(img.get_array()[:,:,position])[::-1]

    return slice_img


def plot_image(figure, img, projection='max_intensity', orientation=None, colormap='gray', value_range=None, alpha=1.):
    if value_range is None:
        value_range = (img.min(),img.max())

    if projection == 'max_intensity':
        projected_img = np.transpose(img.get_array().max(axis=2))[::-1]
    elif projection == 'surface':
        projected_img, coords = image_surface_projection(img, orientation=orientation)
        projected_img = np.transpose(projected_img)[::-1]

    figure.gca().imshow(projected_img, interpolation='none', alpha=alpha,
                        cmap=colormap, vmin=value_range[0], vmax=value_range[1],
                        extent=(0, img.extent[0], 0, img.extent[1]))


def plot_image_slice(figure, img, axis='x', position=None, orientation=None, colormap='gray', value_range=None, alpha=1.):
    if value_range is None:
        value_range = (img.min(),img.max())
    if orientation is None:
        orientation = 1 - 2 * (np.mean(img[:, :, 0]) > np.mean(img[:, :, -1]))

    slice_img = image_slice(img, axis=axis, position=position)
    if axis=='x':
        extent = (0, img.extent[1], 0, img.extent[2])
    elif axis=='y':
        extent = (0, img.extent[0], 0, img.extent[2])
    elif axis=='z':
        extent = (0, img.extent[0], 0, img.extent[1])
        orientation = -1

    figure.gca().imshow(slice_img[::-orientation], interpolation='none', alpha=alpha,
                        cmap=colormap, vmin=value_range[0], vmax=value_range[1],
                        extent=extent)


def plot_image_rgb_blend(figure, red_img=None, green_img=None, blue_img=None, projection='max_intensity', orientation=None):
    """

    Parameters
    ----------
    figure
    red_img
    green_img
    blue_img

    Returns
    -------

    None
    """
    if green_img is None and red_img is not None:
        green_img = SpatialImage(np.zeros_like(red_img.get_array()),voxelsize=red_img.voxelsize)
    if red_img is None and green_img is not None:
        red_img = SpatialImage(np.zeros_like(green_img.get_array()),voxelsize=green_img.voxelsize)
    if blue_img is None and green_img is not None:
        blue_img = SpatialImage(np.zeros_like(green_img.get_array()),voxelsize=green_img.voxelsize)

    if projection == 'max_intensity':
        rgb_img = np.transpose([red_img.get_array().max(axis=2),
                                green_img.get_array().max(axis=2),
                                blue_img.get_array().max(axis=2)], (2, 1, 0))[::-1]
    elif projection == 'surface':
        mean_img = SpatialImage(np.mean([red_img.get_array(), green_img.get_array(), blue_img.get_array()],axis=0),
                                voxelsize=green_img.voxelsize)
        _, coords = image_surface_projection(mean_img,orientation=orientation)
        rgb_img = np.transpose([red_img.get_array()[coords].reshape(red_img.shape[:2]),
                                green_img.get_array()[coords].reshape(green_img.shape[:2]),
                                blue_img.get_array()[coords].reshape(blue_img.shape[:2])], (2, 1, 0))[::-1]
    else:
        rgb_img = None
    figure.gca().imshow(rgb_img, extent=(0, green_img.extent[0], 0, green_img.extent[1]))


def contour_cells(figure, seg_img_2D, extent, cell_property=None, alpha=0.5, colormap='glasbey', edgecolor='k', linewidth=0.5, value_range=None):
    if value_range is None:
        if cell_property is None:
            value_range = (0, 255)
        else:
            value_range = (np.nanmin(list(cell_property.values())),
                           np.nanmax(list(cell_property.values())))

    labels = [c for c in np.unique(seg_img_2D) if c != 1]
    mpl_cmap = ScalarMappable(cmap=colormap, norm=Normalize(*value_range))
    for c in labels:
        if cell_property is None:
            color = mpl_cmap.to_rgba(c % 256)
        else:
            if c in cell_property.keys():
                color = mpl_cmap.to_rgba(cell_property[c])
            else:
                color = 'none'
        cell_mask = (seg_img_2D == c)[::-1]
        figure.gca().contour(cell_mask,
                             levels=[0.5],
                             colors=[edgecolor], alpha=alpha, linewidths=[linewidth],
                             extent=extent)
        figure.gca().contourf(cell_mask,
                              levels=[0.5, 1],
                              colors=[color], alpha=alpha/2.,
                              extent=extent)


def plot_image_surface_segmentation(figure, seg_img, img=None, orientation=None, cell_property=None, colormap='glasbey', value_range=(0,255), plot_type='contour', plot_labels=False, alpha=0.5):
    if img is None:
        projected_seg_img, coords = image_surface_projection(seg_img, gaussian_sigma=0, threshold=1, orientation=orientation)
    else:
        projected_img, coords = image_surface_projection(img, orientation=orientation)
        projected_seg_img = seg_img.get_array()[coords].reshape(seg_img.shape[:2])

    if plot_type == 'contour':
        figure.gca().imshow(np.transpose(projected_seg_img%256)[::-1], alpha=0,
                            extent=(0, seg_img.extent[0], 0, seg_img.extent[1]))
        contour_cells(figure, np.transpose(projected_seg_img)[::-1],
                      extent=(0, seg_img.extent[0], 0, seg_img.extent[1]), alpha=alpha,
                      cell_property=cell_property, colormap=colormap, value_range=value_range)
    elif plot_type == 'image':
        if cell_property is None:
            projected_property_img = (projected_seg_img%256).astype(float)
            projected_property_img[projected_seg_img==1] = np.nan
        else:
            cell_property[1] = np.nan
            projected_property_img = array_dict(cell_property).values(projected_seg_img)
        figure.gca().imshow(np.transpose(projected_property_img)[::-1], cmap=colormap, vmin=value_range[0], vmax=value_range[1],
                            interpolation='none',  extent=(0, seg_img.extent[0], 0, seg_img.extent[1]))

    if plot_labels:
        labels = [l for l in np.unique(projected_seg_img) if l!=1]
        label_center = dict(zip(labels, nd.center_of_mass(np.ones_like(projected_seg_img), projected_seg_img, index=labels)))
        label_center = {l:label_center[l]*np.array(seg_img.voxelsize)[:2] for l in labels}
        for l in labels:
            figure.gca().annotate(str(l), label_center[l][:2], size=8, ha='center', va='center')


def plot_segmentation_slice(figure, seg_img, axis='x', position=None, orientation=None, cell_property=None, colormap='glasbey', value_range=None, plot_type='contour', plot_labels=False, alpha=0.5):
    if orientation is None:
        orientation = 1 - 2*(np.mean(seg_img[:,:,0]) > np.mean(seg_img[:,:,-1]))

    slice_seg_img = image_slice(seg_img, axis=axis, position=position)
    if axis == 'x':
        extent = (0, seg_img.extent[1], 0, seg_img.extent[2])
    elif axis == 'y':
        extent = (0, seg_img.extent[0], 0, seg_img.extent[2])
    elif axis == 'z':
        extent = (0, seg_img.extent[0], 0, seg_img.extent[1])
        orientation = -1

    if plot_type == 'contour':
        figure.gca().imshow(slice_seg_img[::-orientation], alpha=0, extent=extent)
        contour_cells(figure, slice_seg_img[::-orientation], extent, alpha=alpha,
                      cell_property=cell_property, colormap=colormap, value_range=value_range)
    elif plot_type == 'image':
        if cell_property is None:
            slice_property_img = (slice_seg_img%256).astype(float)
            slice_property_img[slice_seg_img==1] = np.nan
        else:
            cell_property[1] = np.nan
            slice_property_img = array_dict(cell_property).values(slice_seg_img)

        figure.gca().imshow(slice_property_img[::-orientation],
                            cmap=colormap, vmin=value_range[0], vmax=value_range[1],
                            interpolation='none', extent=extent, alpha=alpha)

    if plot_labels:
        labels = [l for l in np.unique(slice_seg_img) if l!=1]
        label_center = dict(zip(labels, nd.center_of_mass(np.ones_like(slice_seg_img), slice_seg_img, index=labels)))
        label_center = {l:label_center[l]*np.array(seg_img.voxelsize)[:2] for l in labels}
        for l in labels:
            figure.gca().annotate(str(l), label_center[l][:2], size=8, ha='center', va='center')
