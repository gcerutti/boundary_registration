import logging
from time import time as current_time

import numpy as np
import scipy.ndimage as nd
from scipy.spatial.distance import cdist

from skimage.measure import marching_cubes


# timagetk imports :
# ------------------

from timagetk.components import SpatialImage, LabelledImage

def compute_surfel_neighbors(seg_img):
    start_time = current_time()
    sh = np.array(seg_img.shape)

    xy_surfel_img = []
    for z in np.arange(-1, 1):
        xy_surfel_img.append(seg_img[:, :, 1 + z:sh[2] + z])
    xy_surfel_img = np.sort(np.transpose(xy_surfel_img, (1, 2, 3, 0))).reshape(sh[0] * sh[1] * (sh[2] - 1), 2)

    non_flat = np.sum(xy_surfel_img == np.tile(xy_surfel_img[:, :1], (1, 2)), axis=1) != 2
    xy_surfel_img = xy_surfel_img[non_flat]

    xz_surfel_img = []
    for y in np.arange(-1, 1):
        xz_surfel_img.append(seg_img[:, 1 + y:sh[1] + y, :])
    xz_surfel_img = np.sort(np.transpose(xz_surfel_img, (1, 2, 3, 0))).reshape(sh[0] * (sh[1] - 1) * sh[2], 2)

    non_flat = np.sum(xz_surfel_img == np.tile(xz_surfel_img[:, :1], (1, 2)), axis=1) != 2
    xz_surfel_img = xz_surfel_img[non_flat]

    yz_surfel_img = []
    for x in np.arange(-1, 1):
        yz_surfel_img.append(seg_img[1 + x:sh[0] + x, :, :])
    yz_surfel_img = np.sort(np.transpose(yz_surfel_img, (1, 2, 3, 0))).reshape((sh[0] - 1) * sh[1] * sh[2], 2)

    non_flat = np.sum(yz_surfel_img == np.tile(yz_surfel_img[:, :1], (1, 2)), axis=1) != 2
    yz_surfel_img = yz_surfel_img[non_flat]

    surfel_img = []
    surfel_img += list(xy_surfel_img)
    surfel_img += list(xz_surfel_img)
    surfel_img += list(yz_surfel_img)
    surfel_img = np.array(surfel_img)

    cell_adjacency_edges = np.unique(surfel_img ,axis=0)

    cell_neighbors = {l :set() for l in np.unique(seg_img)}
    for c1 ,c2 in cell_adjacency_edges:
        cell_neighbors[c1] |= {c2}
        cell_neighbors[c2] |= {c1}

    logging.info("Computing cell neighbors [" + str(current_time() - start_time) + "s]")

    return cell_neighbors


def compute_layers(seg_img, cell_neighbors=None):
    """
    Gives a dictionnary with keys the labels and values the estimated layer.
    """
    start_time = current_time()
    if not isinstance(seg_img, LabelledImage):
        seg_img = LabelledImage(seg_img, no_label_id=0)

    cell_labels = np.array([l for l in seg_img.labels() if l!= 1])
    if cell_neighbors is None:
        cell_neighbors = compute_surfel_neighbors(seg_img)
    background_neighbors = list(cell_neighbors[1])

    cell_l1 = np.array([c in background_neighbors for c in cell_labels])
    l1_cells = cell_labels[cell_l1]
    l2_cells = [c for c in cell_labels
                if np.any([n in l1_cells for n in cell_neighbors[c]])
                and not c in l1_cells]
    l3_cells = [c for c in cell_labels
                if np.any([n in l2_cells for n in cell_neighbors[c]])
                and not (c in l1_cells or c in l2_cells)]
    cell_layer = {c: 1 if c in l1_cells else 2 if c in l2_cells else 3 if c in l3_cells else 4 for c in cell_labels}

    logging.info("Computing cell layers [" + str(current_time() - start_time) + "s]")

    return cell_layer


def compute_centers(seg_img, real=False):
    """
    Gives a dictionnary with keys the labels and values the cell centers.
    """
    start_time = current_time()
    labels = np.array([l for l in np.unique(seg_img) if l != 1])
    centers = dict(zip(labels, nd.center_of_mass(np.ones_like(seg_img), seg_img, index=labels)))
    if real:
        centers = {c: p * np.array(seg_img.voxelsize) for c, p in centers.items()}
    logging.info("Computing cell centers [" + str(current_time() - start_time) + "s]")

    return centers


def compute_volumes(seg_img, real=False):
    """
    Gives a dictionnary with keys the labels and values the volumes.
    """
    start_time = current_time()
    labels = np.unique(seg_img)
    volumes = dict(zip(labels, nd.sum(np.ones_like(seg_img), seg_img, index=labels)))
    if real:
        volumes = {c:v*np.prod(seg_img.voxelsize) for c,v in volumes.items()}
    logging.info("Computing cell volumes [" + str(current_time() - start_time) + "s]")

    return volumes


def removeAll(the_list,val):
	while val in the_list:
		the_list.remove(val)

def ExtendSlice(sl,s,marge):
	return slice(max(0,sl.start-marge),min(s,sl.stop+marge),sl.step)	

def ExtendCube(cube,s,marge):
	d=len(s)
	ext=[]
	for i in range(0,d):
		ext.append(ExtendSlice(cube[i],s[i],marge))
	ext=tuple(ext)
	return ext


def compute_surfaces(seg_img, real=False) :
	"""
	Gives a dictionnary with keys the labels and values the surface of the objects having this labels.
	"""
	start_time = current_time()
	labels = np.unique(seg_img)
	labels=[i for i in labels if i>0]
	bounding_box=nd.find_objects(seg_img)
	removeAll(bounding_box,None)
	wallvolumes=[]
	dtype=seg_img.dtype
	s=seg_img.shape
	marge=5
	if real:
		thickness = (np.prod(seg_img.voxelsize))**(1./3.)
	else:
		thickness=1.
	for i in range(len(labels)) :
		box=bounding_box[i]
		box=ExtendCube(box,s,marge)
		seg_slice=np.array(seg_img[box])
		seg_mask=(seg_slice==labels[i]).astype(dtype)
		seg_dilated=nd.binary_dilation(seg_mask,iterations=1)
		wall = seg_dilated - seg_mask
		wallvolumes.append(nd.sum(wall)*thickness**2)
	# correction : Sth=Snun/0.72
	wallvolumes = [i/0.8 for i in wallvolumes]
	surfaces = dict(zip(labels, wallvolumes))
	logging.info("Computing surfaces [" + str(current_time() - start_time) + "s]")
	surfaces[0]=0
	return surfaces

def compute_surface_marching_cubes(seg_img, real=True):
    cell_labels = np.unique(seg_img)
    cell_bboxes = nd.find_objects(seg_img)
    cell_bboxes = {c+1: bbox for c,bbox in enumerate(cell_bboxes) if bbox is not None}
    for c in cell_labels:
        cell_bboxes[c] = tuple([slice(np.maximum(cell_bboxes[c][i].start-1, 0),
                                      np.minimum(cell_bboxes[c][i].stop+1, seg_img.shape[i]),
                                      None)
                                for i, dim in enumerate('zyx')])
    cell_images = [seg_img[cell_bboxes[l]]==l for l in cell_labels]

    cell_meshes = {}
    for c, cell_img in zip(cell_labels, cell_images):
        cell_points, cell_triangles, _, _ = marching_cubes(cell_img, level=0.5)
        cell_points += np.array([cell_bboxes[c][i].start for i, dim in enumerate('zyx')])
        if real:
            cell_points *= np.array(seg_img.voxelsize)
        cell_meshes[c] = (cell_points, cell_triangles)

    triangle_edge_list  = np.array([[1, 2], [2, 0], [0, 1]])
    cell_surfaces = {0: 0.}
    for c in cell_meshes:
        points, triangles = cell_meshes[c]
        triangle_edges = triangles[:, triangle_edge_list]
        triangle_edge_points = points[triangle_edges]
        triangle_edge_lengths = np.linalg.norm(triangle_edge_points[:,:,1] - triangle_edge_points[:,:,0], axis=-1)

        triangle_perimeters = triangle_edge_lengths.sum(axis=1)
        triangle_heron = triangle_perimeters/2.
        triangle_heron *= triangle_perimeters/2. - triangle_edge_lengths[:,0]
        triangle_heron *= triangle_perimeters/2. - triangle_edge_lengths[:,1]
        triangle_heron *= triangle_perimeters/2. - triangle_edge_lengths[:,2]
        triangle_areas = np.sqrt(np.maximum(0, triangle_heron))
        cell_surfaces[c] = triangle_areas.sum()

    return cell_surfaces

def compute_diameters(seg_img, real=False) :
	"""
	Gives a dictionnary with keys the labels and values the maximal diameter.
	"""
	start_time = current_time()
	labels = np.unique(seg_img)
	labels=[i for i in labels if i>0]
	bounding_box=nd.find_objects(seg_img)
	removeAll(bounding_box,None)
	Dmax_list=[]
	dtype=seg_img.dtype
	s=seg_img.shape
	marge=5
	vs=1.
	if real:
		vs = (np.prod(seg_img.voxelsize))**(1./3.)
	for i in range(len(labels)) :
		box=bounding_box[i]
		box=ExtendCube(box,s,marge)
		seg_slice=np.array(seg_img[box])
		seg_mask=(seg_slice==labels[i]).astype(dtype)
		seg_dilated=nd.binary_dilation(seg_mask,iterations=1)
		wall = seg_dilated - seg_mask
		wallcoord = np.where(wall)
		distances = cdist(wallcoord, wallcoord)
		dist=np.unique(distances)[-1]*vs
		Dmax_list.append(dist)
	Dmax = dict(zip(labels, Dmax_list))
	logging.info("Computing maximal diameter [" + str(current_time() - start_time) + "s]")
	Dmax[0]=0
	return Dmax
