# This is a library of functions useful for MARS
# it is based on the timagetk python module
# https://mosaic.gitlabpages.inria.fr/timagetk

import logging
from time import time as current_time

import numpy as np
import scipy.ndimage as nd
# import mahotas


# timagetk imports :
# ------------------

from timagetk.components import SpatialImage, LabelledImage

# filtering
from timagetk.plugins import linear_filtering, morphology

# segmentation
from timagetk.plugins import h_transform, region_labeling
from timagetk.algorithms import watershed as watershed_titk

# local imports :
# ---------------

from titk_tools.quantification import compute_volumes

# ========================
# Segmentation
# ========================


def gauss_titk(img, radius):
    """Gaussian filter with radius in real units

    - if radius=a number : isotropic
    - if radius= a list of numbers : anisotropic

    Parameters
    ----------
    img
    radius

    Returns
    -------

    """
    img_filtered = linear_filtering(img, std_dev=radius, method='gaussian_smoothing', real=True)
    return img_filtered


def filtering(img, filter_type, filter_value, real=False):
    if filter_type == 'gaussian':
        start_time = current_time()
        # img_filtered = linear_filtering(img, std_dev=filter_value, method='gaussian_smoothing')
        voxelsize = np.array(img.voxelsize)
        if real:
            sigma = filter_value / voxelsize
        else:
            sigma = [filter_value for _ in voxelsize]
        img_filtered = nd.gaussian_filter(img, sigma=sigma).astype(img.dtype)
        img_filtered = SpatialImage(img_filtered, voxelsize=voxelsize)
        logging.info("Smoothing image (gaussian) [" + str(current_time() - start_time) + "s]")
    elif filter_type == 'asf':
        start_time = current_time()
        img_filtered = img
        if real:
            filter_value = int(filter_value / np.mean(img.voxelsize))
        filter_value = int(filter_value)
        if filter_value > 0:
            for i in range(filter_value):
                img_filtered = morphology(img_filtered, method='oc_alternate_sequential_filter')
        logging.info("Smoothing image (asf) [" + str(current_time() - start_time) + "s]")
    else:
        logging.warning('Unknown filter_type...')
    return img_filtered


def seed_extraction(img, h_minima):
    # local minima
    start_time = current_time()
    # reg_min_image = h_transform(img, h=h_minima, method='h_transform_min')
    reg_min_image = h_transform(img, h=h_minima, method='min')
    logging.info("Computing H-transform [" + str(current_time() - start_time) + "s]")

    # labeling of connexe components
    start_time = current_time()
    seed_img = region_labeling(reg_min_image, low_threshold=1, high_threshold=h_minima, method='connected_components')
    logging.info("Extracting seed image [" + str(current_time() - start_time) + "s]")

    return seed_img


def put_background_value(wat_in, label):
    # background is identified as the biggest object in the segmentation
    vols = compute_volumes(wat_in)
    max_volume_index = np.argmax(list(vols.values()))
    bg_vol = list(vols.values())[max_volume_index]
    bg = list(vols.keys())[max_volume_index]

    if bg != label:
        wat = SpatialImage(wat_in.copy(), voxelsize=wat_in.voxelsize, origin=wat_in.origin )
        if label in vols.keys():
            #swap the two labels, with intermediary label= max+1
            inter = wat.max()+1
            wat[wat == label] = inter
            wat[wat == bg] = label
            wat[wat == inter] = bg
        else :
            wat[wat == bg] = label
    else:
        wat = wat_in
    return wat


def watershed(img, seed_img):
    start_time = current_time()
    seg_img = watershed_titk(img, seed_img, param_str_1='-l first')
    logging.info("Performing watershed segmentation [" + str(current_time() - start_time) + "s]")

    # put label=1 to the biggest cell (background)
    start_time = current_time()
    new_seg_img = put_background_value(seg_img, 1)
    logging.info("Assigning label 1 to the largest cell [" + str(current_time() - start_time) + "s]")

    return new_seg_img


def is_segmented(img):
    return np.sum(img[:-1] == img[1:]) > 0.5


def remove_small_cells(seg_img, seed_img, vol_min=None, real=False, relabel=True):
    """seed_img corresponding to cells with a volume less then vol_min are removed.

    Parameters
    ----------
    seg_img: SpatialImage
        segmented image
    seed_img: SpatialImage
        labeled markers image
    vol_min: int
        minimum volume
    real: bool, optional
        If True, volume is in real-world units else in voxels (default)
    relabel: bool, optional
        If True, the seed_img are relabeled so that removed seed_img do not cause
        "holes" in the labeling

    Returns
    -------
    new_seed_img: SpatialImage
        the new seed image
    """
    volumes = compute_volumes(seg_img)
    # do not modify the background (labeled by 1)
    volumes.pop(1)
    new_seed_img = SpatialImage(seed_img.copy(), voxelsize=seed_img.voxelsize, origin=seed_img.origin)
    if vol_min is None:
        vol_min = np.mean(list(volumes.values())) - 2*np.std(list(volumes.values()))
    for label, vol in volumes.items():
        if vol < vol_min:
            logging.info("Removing cell {}, volume  {}".format(label,volumes[label]))
            new_seed_img[new_seed_img == label] = 0
    if relabel:
        regions = SpatialImage(new_seed_img > 0, voxelsize=seg_img.voxelsize, origin=seg_img.origin, dtype='uint8')
        # relabeling of connexe components
        new_seed_img = region_labeling(regions, low_threshold=1, high_threshold=1, method='connected_components')
    return new_seed_img


def merge_large_cells(seg_img, vol_max=None, background_label=1):
    new_seg_img = SpatialImage(seg_img.copy(), voxelsize=seg_img.voxelsize, origin=seg_img.origin)
    if vol_max is not None:
        volumes = compute_volumes(new_seg_img)
        labels_to_remove = np.array(list(volumes.keys()))[np.array(list(volumes.values())) > vol_max]
        for l in labels_to_remove:
            if l != background_label:
                logging.info("Merging cell {} to background, volume  {}".format(l,volumes[l]))
                new_seg_img[new_seg_img == l] = background_label
    return new_seg_img


def segmentation_watershed(img_seed, img_wat, h_minima, voxel_volmin, voxel_volmax=None, real=False):
    #searching the seed_img
    seed_img = seed_extraction(img_seed, h_minima)
    # first watershed
    wat = watershed(img_wat, seed_img)
    #removing small cells
    if real:
        voxel_volmin = voxel_volmin/np.prod(img_wat.voxelsize)
    new_seed_img = remove_small_cells(wat, seed_img, voxel_volmin)
    # new watershed
    wat = watershed(img_wat, new_seed_img)

    if voxel_volmax is not None:
        if real:
            voxel_volmax = voxel_volmax/np.prod(img_wat.voxelsize)
        wat = merge_large_cells(wat, voxel_volmax, background_label=1)

    return wat, new_seed_img


def segmentation_watershed_from_seeds(seed_img, img_wat, voxel_volmin, voxel_volmax=None, real=False):
    # first watershed
    wat = watershed(img_wat, seed_img)
    #removing small cells
    if real:
        voxel_volmin = voxel_volmin/np.prod(img_wat.voxelsize)
    new_seed_img = remove_small_cells(wat, seed_img, voxel_volmin)
    # new watershed
    wat = watershed(img_wat, new_seed_img)

    if voxel_volmax is not None:
        if real:
            voxel_volmax = voxel_volmax/np.prod(img_wat.voxelsize)
        wat = merge_large_cells(wat, voxel_volmax, background_label=1)

    return wat, new_seed_img


def most_frequent_below_tissuesurface(img, outspace):
    # estimating most frequent intensity value inside the tissue, near the surface
    inspace = complementary(outspace)
    inspace_top = (nd.binary_erosion(inspace,iterations=1)).astype('uint8')
    inspace_bottom = (nd.binary_erosion(inspace_top,iterations=10)).astype('uint8')
    estimation_mask = inspace_top-inspace_bottom
    coords = np.where(estimation_mask==1)
    zone_values = [ img[coords[0][i],coords[1][i],coords[2][i]]  for i in range(0,len(coords[0]))]
    return np.argmax(np.bincount(zone_values))


# def complementary(lsm) :
#     """
#     If lsm is a binary image, it gives its complementary image
#     """
#     compl = np.ones_like(lsm)
#     return compl-lsm


# def fill_holes(mask):
#     """
#     eliminates holes inside an object defined by a binary image
#     """
#     outside = complementary(mask)
#     # we want to eliminate cell-middles cathed by the threshold
#     seeds,_ = mahotas.label(outside)
#     # 0 labels the "tissue", 1 labels the outside, other labels correspond to catched holes
#     print("mask.shape=",mask.shape)
#     if len(mask.shape)==3 :
#         out_label = seeds[-1,-1,0]
#     elif len(mask.shape)==2 :
#         out_label = seeds[-1,-1]
#         #out_label = seeds[0,0]
#     else :
#         print("wrong dimension for the object to fill (2 or 3)")
#     #outside = SpatialImage((seeds==out_label).astype('uint8'), voxelsize=mask.voxelsize, origin=mask.origin)
#     outside = (seeds==out_label).astype('uint8')
#     inside = complementary(outside)
#     return inside


# def fill_holes_bottom(mask, zlevel='no', outside=True):
#     """
#     Consider an object defined by a binary image. The goal is to eliminate
#     all interior holes, as well as those which communicate with the bottom.
#     it adds a compact slice at zlevel in the interior of the contour.
#     Then it eliminates holes inside the object defined by a binary image
#     """
#     if outside:
#         mask = complementary(mask)
#     # now tissue is label 1, background is label 0
#     s = mask.shape
#     # consider the slice at zlevel
#     if zlevel=='no' :
#         zlevel = s[2]-1# bottom is at maximal z coordinate, if no slice-number is specified
#     print(zlevel)
#     mask2D = mask[:,:,zlevel]
#     # fill in holes in it
#     mask2D = fill_holes(mask2D)
#     # put it back in the image as "coverslip"
#     mask[:,:,zlevel] = mask2D
#     # and now fill in holes in 3D
#     mask = fill_holes(mask)
#     if outside:
#         mask=complementary(mask)
#     return mask


def complementary(binary_img):
    """Gives the complementary of a binary image.

    Parameters
    ----------
    binary_img : numpy.ndarray
        A binary image array (with 0 and 1 values, ideally).

    Returns
    -------
    compl: numpy.ndarray
        The complementary binary image array (with 0 and 1 values).

    """
    compl = 1 - (binary_img > 0)
    return compl


def fill_holes(binary_img, criterion='corner', orientation=1):
    """Eliminates holes inside the object defined by a binary image.

    Operates by computing the connected components of background elements
    and keeping only one as the background region. Everything else is
    considered as object pixels, hence filling the holes that could exist
    inside the object.

    Parameters
    ----------
    binary_img : numpy.ndarray
        A binary image array (with 0 and 1 values, ideally).
    criterion : str
        Whether to select the background by size or corner position.
    orientation : int
        Whether to look at the top (1) or bottom (-1) of the stack.

    Returns
    -------
    inside : numpy.ndarray
        A binary image array where holes have been filled.

    """
    outside = complementary(binary_img)
    seeds, n_seeds = nd.label(outside)

    if criterion == 'corner':
        if binary_img.ndim == 3:
            out_label = seeds[-1, -1, 0 if orientation == 1 else -1]
        elif binary_img.ndim == 2:
            out_label = seeds[-1, -1]
    elif criterion == 'size':
        seed_size = nd.sum(np.ones_like(seeds), seeds, np.arange(1, n_seeds+1))
        out_label = np.arange(1, n_seeds+1)[np.argmax(seed_size)]
    else:
        out_label = 0

    inside = (seeds != out_label).astype(np.uint8)
    return inside


def fill_holes_bottom(binary_img, zlevel=None, outside=True, criterion='corner', orientation=1):
    """Eliminates all holes, even if they communicate with the bottom.

    Adds a compact slice at zlevel in the interior of the contour, then
    eliminates holes inside the object defined by a binary image.

    Parameters
    ----------
    binary_img : numpy.ndarray
        A binary image array (with 0 and 1 values, ideally).
    zlevel : int
        The position in z at which to add a compact slice.
    outside : bool
        Whether the input image represents actually a background.
    criterion : str
        Whether to select the background by size or corner position.
    orientation : int
        Whether to look at the top (1) or bottom (-1) of the stack.

    Returns
    -------
    inside : numpy.ndarray
        A binary image array where holes have been filled.

    """

    if outside:
        inside = complementary(binary_img)
    else:
        inside = np.copy(binary_img)

    if zlevel is None:
        zlevel = -1 if orientation==1 else 0
    inside[:,:,zlevel] = fill_holes(inside[:,:,zlevel], criterion='corner')
    inside = fill_holes(inside, criterion=criterion, orientation=orientation)

    if outside:
        return complementary(inside)
    else:
        return inside
