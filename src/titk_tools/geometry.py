import logging
from time import time as current_time

import numpy as np
import scipy.ndimage as nd

# timagetk imports :
# ------------------

from timagetk.components import SpatialImage, LabelledImage
from timagetk.plugins.resampling import isometric_resampling

# mesh imports :
# ------------------

from cellcomplex.property_topomesh.analysis import compute_topomesh_property, compute_topomesh_vertex_property_from_faces
from cellcomplex.property_topomesh.optimization import topomesh_triangle_split, property_topomesh_vertices_deformation

from tissue_nukem_3d.nuclei_mesh_tools import nuclei_image_surface_topomesh, up_facing_surface_topomesh

# local imports :
# ---------------

from titk_tools.quantification import compute_centers, compute_surfel_neighbors


curvature_properties = ['mean_curvature', 'gaussian_curvature', 'principal_curvature_min', 'principal_curvature_max']


def _segmentation_surface_mesh(seg_img, background=1, resampling_voxelsize=1., maximal_length=5., microscope_orientation=None, compute_curvature=False, curvature_neighborhood=3):
    if microscope_orientation is None:
        microscope_orientation = 1 - 2*(np.mean(seg_img[:,:,0]) > np.mean(seg_img[:,:,-1]))

    binary_img = SpatialImage(nd.gaussian_filter(255*(seg_img != background), sigma=1./np.array(seg_img.voxelsize)).astype(np.uint8), voxelsize=seg_img.voxelsize)

    if resampling_voxelsize is not None:
        binary_img = isometric_resampling(binary_img, method=resampling_voxelsize, option='linear')
    else:
        resampling_voxelsize = np.array(binary_img.voxelsize)

    surface_topomesh = nuclei_image_surface_topomesh(binary_img,
                                                     nuclei_sigma=resampling_voxelsize,
                                                     density_voxelsize=resampling_voxelsize,
                                                     maximal_length=2*maximal_length,
                                                     intensity_threshold=64,
                                                     padding=False,
                                                     decimation=100)

    surface_topomesh = up_facing_surface_topomesh(surface_topomesh, normal_method='orientation',
                                                  upwards=microscope_orientation==1,
                                                  down_facing_threshold=0)

    surface_topomesh = topomesh_triangle_split(surface_topomesh)
    property_topomesh_vertices_deformation(surface_topomesh, omega_forces={'taubin_smoothing':0.66}, iterations=10)

    compute_topomesh_property(surface_topomesh, 'normal', 2, normal_method='orientation')
    compute_topomesh_vertex_property_from_faces(surface_topomesh, 'normal', neighborhood=curvature_neighborhood, adjacency_sigma=0.4*curvature_neighborhood)

    if compute_curvature:
        compute_topomesh_property(surface_topomesh, 'mean_curvature', 2)
        for property_name in curvature_properties:
            compute_topomesh_vertex_property_from_faces(surface_topomesh, property_name, neighborhood=curvature_neighborhood, adjacency_sigma=0.4*curvature_neighborhood)
        surface_topomesh.update_wisp_property('gaussian_curvature', 0, {
            v: surface_topomesh.wisp_property('principal_curvature_min', 0)[v]*surface_topomesh.wisp_property('principal_curvature_max', 0)[v]
            for v in surface_topomesh.wisps(0)
        })

    return surface_topomesh


def compute_surface_curvature(seg_img, orientation=None, l1_distance_threshold=7., surface_topomesh=None, cell_neighbors=None):
    start_time = current_time()
    cell_labels = np.array([l for l in np.unique(seg_img) if l != 1])
    cell_centers = compute_centers(seg_img, real=True)

    if orientation is None:
        orientation = 1 - 2*(np.mean(seg_img[:,:,0]) > np.mean(seg_img[:,:,-1]))

    if surface_topomesh is None:
        surface_topomesh = _segmentation_surface_mesh(seg_img, microscope_orientation=-orientation, compute_curvature=True)

    cell_points = np.array([cell_centers[c] for c in cell_labels])
    surface_points = surface_topomesh.wisp_property('barycenter', 0).values(list(surface_topomesh.wisps(0)))
    surface_distances = np.linalg.norm(cell_points[:, np.newaxis] - surface_points[np.newaxis], axis=2)

    cell_surface_vertex = np.array(list(surface_topomesh.wisps(0)))[np.argmin(surface_distances, axis=1)]
    cell_surface_distances = np.min(surface_distances, axis=1)

    if cell_neighbors is None:
        cell_neighbors = compute_surfel_neighbors(seg_img)
    background_neighbors = list(cell_neighbors[1])
    cell_l1 = np.array([(d < l1_distance_threshold) & (c in background_neighbors) for c, d in zip(cell_labels, cell_surface_distances)])

    l1_cells = cell_labels[cell_l1]
    l1_cell_surface_vertex = cell_surface_vertex[cell_l1]

    cell_curvature = {}
    for property_name in curvature_properties:
        l1_cell_curvature = dict(zip(l1_cells, surface_topomesh.wisp_property(property_name, 0).values(l1_cell_surface_vertex)))
        cell_curvature[property_name] = {c: l1_cell_curvature[c] if c in l1_cells else np.nan for c in cell_labels}

    logging.info("Computing surface cell curvature [" + str(current_time() - start_time) + "s]")

    return cell_curvature