from titk_tools.io import imread, imsave, tfsave, tfread, read_trsf, save_trsf, matrix2tf

from titk_tools.registration import isotropic_resampling as _isotropic_resampling
from titk_tools.registration import isotropic_resampling_seg as _isotropic_resampling_seg

from titk_tools.registration import find_rigid_transfo2 as _find_rigid_transfo2
from titk_tools.registration import find_rigid_transfo as _find_rigid_transfo
from titk_tools.registration import find_affine_transfo as _find_affine_transfo
from titk_tools.registration import find_nl_transfo as _find_nl_transfo

from titk_tools.registration import apply_transfo_on_seg1 as _apply_transfo_on_seg1
from titk_tools.registration import tf2vf

import time
import numpy as np
from scipy import ndimage as nd
import os

# registering and fusing
from timagetk.plugins import averaging

# ========================
# Resampling
# ========================

def isotropic_resampling(im_filename, iso_filename, isores):
	im = imread(im_filename)
	im_out = _isotropic_resampling(im, isores)
	imsave(iso_filename, im_out)
	return im_out

def isotropic_resampling_seg(im_filename, iso_filename, isores):
	im = imread(im_filename)
	im_out = _isotropic_resampling_seg(im, isores)
	imsave(iso_filename, im_out)
	return im_out


# ========================
# Registration
# ========================


def find_rigid_transfo(path_ref, path_flo, path_res, path_rigid):
    """

    Parameters
    ----------
    path_ref
    path_flo
    path_res
    path_rigid

    Returns
    -------

    """
    print("rigid registration... result saved in :", path_res)
    im_ref = imread(path_ref)
    im_flo = imread(path_flo)
    im_res, trsf_out = _find_rigid_transfo2(im_ref, im_flo)
    imsave(path_res, im_res)
    # save_trsf(trsf_out, path_rigid)
    tfsave(path_rigid, trsf_out)
    return


def find_rigid_transfo2(path_ref, path_flo, path_res, path_rigid):
    """

    Parameters
    ----------
    path_ref
    path_flo
    path_res
    path_rigid

    Returns
    -------

    """
    print("rigid registration... result saved in :", path_res)
    im_ref = imread(path_ref)
    im_flo = imread(path_flo)
    im_res, trsf_out = _find_rigid_transfo2(im_ref, im_flo)
    imsave(path_res, im_res)
    # save_trsf(trsf_out, path_rigid)
    tfsave(path_rigid, trsf_out)
    return


def find_rigid_transfo(path_ref, path_flo, path_res, trf_init, path_rigid):
    """

    Parameters
    ----------
    path_ref
    path_flo
    path_res
    trf_init
    path_affine

    Returns
    -------

    """
    print("rigid registration... result saved in :", path_res)
    im_ref = imread(path_ref)
    im_flo = imread(path_flo)
    init_tf = tfread(trf_init)
    # init_tf=read_trsf( trf_init)
    im_res, trsf_out = _find_rigid_transfo(im_ref, im_flo, init_tf)
    imsave(path_res, im_res)
    tfsave(path_rigid, trsf_out)
    # save_trsf(trsf_out, path_affine)
    return


def find_affine_transfo(path_ref, path_flo, path_res, trf_init, path_affine):
    """

    Parameters
    ----------
    path_ref
    path_flo
    path_res
    trf_init
    path_affine

    Returns
    -------

    """
    print("rigid registration... result saved in :", path_res)
    im_ref = imread(path_ref)
    im_flo = imread(path_flo)
    init_tf = tfread(trf_init)
    # init_tf=read_trsf( trf_init)

    im_res, trsf_out = _find_affine_transfo(im_ref, im_flo, init_tf)

    imsave(path_res, im_res)
    tfsave(path_affine, trsf_out)
    # save_trsf(trsf_out, path_affine)
    return


def find_nl_transfo(path_ref, path_flo, path_res, tf_name, ph=5, pl=2, es=3.0, fs=3.0):
    """
    Parameters
    ----------
    path_ref
    path_flo
    path_res
    tf_name
    ph
    pl
    es
    fs

    Returns
    -------

    """
    print("nonlinear registration... result saved in :", path_res)
    im_ref = imread(path_ref)
    im_flo = imread(path_flo)
    im_res, trsf_out = _find_nl_transfo(im_ref, im_flo, ph=ph, pl=pl, es=es, fs=fs)
    imsave(path_res, im_res)
    save_trsf(trsf_out, tf_name)
    return


# ========================
# Transformations
# ========================

def apply_transfo_on_seg1(transfo_file, img_file, transformed_img_file):
	transfo = tfread(transfo_file)
	#transfo =  read_trsf(transfo_file)
	img = imread(img_file)
	img_out = _apply_transfo_on_seg1(transfo, img)
	imsave(transformed_img_file, img_out)
	return

# ========================
# auto-register
# ========================


def average_distance(img1,img2):
	'''
	Gives the mean intensity of the difference image (img2-img1)
	'''
	img1 = img1/img1.mean()
	img2 = img2/img2.mean()
	img21 = np.absolute(img2 - img1)
	img21 = img21.astype('uint8')
	img21 += 10
	img21  = nd.filters.median_filter(img21,size = 2)
	avg_dist = img21.mean()-10
	return avg_dist



def principal_directions(img):	
	'''
	Returns the first two principal eigen vectors of an input image
	Fixed threshold of 10 for a 8 bit image
	It suppposes an isotropic sampling of the image.
	'''
	x,y,z = np.where(img > 10) ## appropriate thresholding 
	x = x - np.mean(x)
	y = y - np.mean(y)
	z = z - np.mean(z)
	coords = np.vstack([x,y,z])
	cov = np.cov(coords)
	evals,evecs = np.linalg.eig(cov)
	sort_indices = np.argsort(evals)[::-1]
	return list(evecs[:,sort_indices][0:2])


def direct_frame(pv):
	'''
	Constructs a direct frame with first two unitvectors v0 and v1
	'''
	return np.array([pv[0], pv[1], np.cross(pv[0], pv[1])])


def write_superposing_transfo(pv_flo, pv_ref, img, trsf_file):
	'''
	- pv_flo and pv-ref are lists of the first two principal vectors of the floating and the reference images respectively
	- img is the floating image
	'''
	# compute the rotation matrix R
	pframe_ref = direct_frame(pv_ref)
	pframe_flo = direct_frame(pv_flo)
	F = np.transpose(pframe_ref)
	G = np.transpose(pframe_flo)
	R = np.matmul(G, np.linalg.inv(F))
	# blockmatching rotates arround the origin,
	# so a rotation arround the middle of the image contains an additional translation t
	s = img.shape
	res = img.voxelsize
	com = np.array(res)*np.array(s)/2
	i = np.identity(3)
	t = np.dot((i-R),np.array(com))
	R = np.transpose(R)
	write_rigid(trsf_file, R, t)
	return


def superposing_transfo(pv_flo, pv_ref, img):
	'''
	- pv_flo and pv-ref are lists of the first two principal vectors of the floating and the reference images respectively
	- img is the floating image
	'''
	# compute the rotation matrix R
	pframe_ref = direct_frame(pv_ref)
	pframe_flo = direct_frame(pv_flo)
	F = np.transpose(pframe_ref)
	G = np.transpose(pframe_flo)
	R = np.matmul(G, np.linalg.inv(F))
	# blockmatching rotates arround the origin,
	# so a rotation arround the middle of the image contains an additional translation t
	s = img.shape
	res = img.voxelsize
	com = np.array(res)*np.array(s)/2
	i = np.identity(3)
	t = np.dot((i-R),np.array(com))
	R = np.transpose(R)
	# construct the rigid transformation matrix T
	T = np.identity(4)
	T[0:3,0:3] = R
	T = np.transpose(T)
	T[0:3,3] = t
	# transform it to bal transformation
	rigid_trsf=matrix2tf(T)
	return rigid_trsf


def write_rigid(trsf_file, R, a):
	'''
	write a rigid transformation with rotation matrix R and translation a
	'''
	T = np.identity(4)
	T[0:3,0:3] = R
	T = np.transpose(T)
	T[0:3,3] = a
	write_transformation(T, trsf_file)
	return


def write_transformation(T, trsf_file):
	'''
	writes the affine transformation T (4x4 matrix) in
	a textfile read by blockmatching.
	'''
	f = open(trsf_file,'w')
	f.write("( "+"\n")
	f.write("08 "+"\n")
	for i in T:
		for j in i:
			f.write("  "+str(j)+" ")
		f.write("\n")
	f.write(") ")
	return

def write_identity(id_file):
	T = np.identity(4)
	write_transformation(T, id_file)
	return


def auto_register(ref_path, flo_path, result_image, result_trsf):
	'''
	- the reference image is already isotropically resampled
	'''
	# fix a "threshold distance";
	# (below this distance the registration quality is accepted)
	tdist = 0.45
	start_function = time.time()
	find_rigid_transfo2(ref_path, flo_path, result_image, result_trsf)
	ref = imread(ref_path)
	tfd_im = imread(result_image)
	dist=average_distance(ref, tfd_im)
	if dist>tdist :	
		tmpdir = 'auto-registration-temporary-files'
		os.system("mkdir "+tmpdir)
		'''	
		isores = ref.voxelsize[0]
		print("Resampling the floating image...")
		flo_resampled_path = tmpdir+'/flo_resampled.inr'
		isotropic_resampling(flo_path, flo_resampled_path, isores)
		flo = imread(flo_resampled_path)
		'''
		flo = imread(flo_path)
		print("Computing the principal vectors...")
		pv_ref = principal_directions(ref)
		print("pv_ref=",pv_ref)
		pv_flo = principal_directions(flo)
		print("pv_flo=",pv_flo)
		print("Computing the initial transformation matrix...")
		# resolve the indeterminancy in orintations : check a total of 4 possibilities (2 for each axis)
		pv_flo10 = np.copy(pv_flo)
		pv_flo10[0] = -pv_flo[0]
		pv_flo01 = np.copy(pv_flo)
		pv_flo01[1] = -pv_flo[1]
		pv_flo11 = -np.copy(pv_flo)
		list_pv = [pv_flo, pv_flo10, pv_flo01, pv_flo11]
		distances = []
		for i in range(0,4):
			print('-----------------')
			trsf_init = tmpdir+'/init_trsf'+str(i)+'.txt'
			trsf_result = tmpdir+'/result_trsf'+str(i)+'.txt'
			trsfd_image = tmpdir+'/trsfd_image'+str(i)+'.inr'
			#write_superposing_transfo(list_pv[i], pv_ref, flo, trsf_init)
			tf_init = superposing_transfo(list_pv[i], pv_ref, flo)
			tfd_im, trsf_res =_find_rigid_transfo(ref, flo, tf_init)
			#find_rigid_transfo(ref_path, flo_path, trsfd_image, trsf_init, trsf_result)
			imsave(trsfd_image, tfd_im)
			#tfd_im = imread(trsfd_image)
			distances.append(average_distance(ref, tfd_im))
		# choose the best orientation
		#print distances
		dist = min(distances)
		imin = distances.index(dist)
		os.system('scp '+tmpdir+'/result_trsf'+str(imin)+'.txt '+result_trsf)
		os.system('scp '+tmpdir+'/trsfd_image'+str(imin)+'.inr '+result_image)
		if distances[imin]>tdist :		
			find_rigid_transfo(ref_path, flo_path, result_image, result_trsf, result_trsf)
			tfd_im = imread(result_image)
			dist = average_distance(ref, tfd_im)
			if dist>tdist :
				print("WARNING!!! The registration is not proper, please check the input image")
		os.system("rm -Rf "+tmpdir)
	print("Registration quality distance=",dist)
	print("Auto-registration done.(time : ",(time.time()-start_function)/60," min)")
	return


# ========================
# fusing images
# ========================

def fuse_images(path_list, mask_list) :
	'''
	fuse images from the path_list, using masks from mask_list
	path_list : list of paths to the images
	mask_list : list of paths to the mask images
	'''
	mask = imread(mask_list[0])
	for i in range(1,len(path_list)):
		mask +=imread(mask_list[i])
	print("adding image ",path_list[0])
	im = imread(path_list[0])
	fused_image = np.zeros_like(im)
	mask_im = imread(mask_list[0])
	informative_voxels = mask.astype(np.bool)
	fused_image[informative_voxels] += (im[informative_voxels]*mask_im[informative_voxels]/mask[informative_voxels]).astype('uint8')
	for i in range(1,len(path_list)):
		print("adding image ",path_list[i])
		im = imread(path_list[i])
		mask_im = imread(mask_list[i])
		fused_image[informative_voxels] += (im[informative_voxels]*mask_im[informative_voxels]/mask[informative_voxels]).astype('uint8')
	#print fused_image.dtype
	fused_image = SpatialImage(fused_image, voxelsize=im.voxelsize, origin=im.origin)
	return fused_image


def fuse_images_titk(path_list) :
	'''
	fuse images from the path_list,	path_list : list of paths to the images
	'''
	im_list =[]
	for i in path_list :
		print("reading",i)
		im_list.append(imread(i))
	rob_mean_image = averaging(im_list, method='mean')#available methods are: ['mean', 'robust_mean', 'median', 'minimum', 'maximum']
	return rob_mean_image



def reconstruction(path_list, mask_list, template, affine_tf_list=[], nl_tf_list=[]) :
	'''
	fuse images from the path_list, using masks from mask_list
	path_list : list of paths to the images
	mask_list : list of paths to the mask images
	template : reference image, useful if resampling has to be performed
	affine_tf_list : list of affine (or rigid) transformations to apply to the masks
	nl_tf_list : list of nonlinear transformations to apply to the masks 
	'''
	mask = imread(mask_list[0])
	np.unique(mask)
	for i in range(1,len(path_list)):
		mask +=imread(mask_list[i])
	# transform images
	temp_list = ['temporary'+str(i)+'.inr' for i in range(0,len(path_list))]
	for i in range(0,len(path_list)):
		os.system('scp '+path_list[i]+' '+temp_list[i])
	for i in range(0,len(path_list)):
		if len(affine_tf_list) == len(path_list) :
			if affine_tf_list[i] != "" :
				apply_transfo_on_image(affine_tf_list[i], temp_list[i], temp_list[i], template)
		if len(nl_tf_list) == len(path_list) :
			if nl_tf_list[i] != "" :
				apply_transfo_on_image(nl_tf_list[i], temp_list[i], temp_list[i], template)
	# fusing the transformed images
	fused_image = fuse_images(temp_list, mask_list)
	for i in range(0,len(path_list)):
		os.system('rm '+temp_list[i])
	return fused_image



def compute_mean_transformation(paths, mean_tf_name) :
	'''
	computes the mean transformation
	'''
	print(paths[0])
	mean_tf = tf2vf(read_trsf(paths[0]))
	for i in range(1,len(paths)):
		tf = tf2vf(read_trsf(paths[i]))
		mean_tf += tf
	mean_tf = mean_tf/len(paths)
	imsave(mean_tf_name, mean_tf)
	return

